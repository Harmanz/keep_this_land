﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public enum aiState
{
    None, Hold,Fight,WaitForEnemy
}
public enum CombatState
{
    Attack,Chasing
}

[System.Serializable]
public class AISkill
{
    public Cast cast;
    public CastCondition condition;
    public Cooldown chanceTimer;

    public bool ReadyToUse(AIController ai)
    {
        if (chanceTimer.cd == 0 || chanceTimer.Check())
            if (condition == null || condition.IsConditionsTrue(ai))
                return true;

        return false;
    }
}

public class AIController : MonoBehaviour
{
    [SerializeField] float aggroRadius = 20f;
    [SerializeField] bool holdPosition = false;
    [SerializeField] AISkill[] skills = null;
    Vector3 startPosition = new Vector3();
    aiState State = aiState.WaitForEnemy;
    public CombatState combatState = CombatState.Chasing;
    Cooldown attemptToGetTargetTimer = new Cooldown(1f);
    Transform target = null;
    MovementController cMovementControl = null;
    CharModifiers cModifiers = null;
    Animator cAnimator = null;
    Combat cCombat = null;
    Team cTeam = null;
    CastController cCastControl = null;

    Dictionary<string,CastSpecificCondition> specificConditions = new Dictionary<string, CastSpecificCondition>();
     Cooldown test = new Cooldown(3f);
    // Start is called before the first frame update
    void Start()
    {
        cCastControl = GetComponent<CastController>();
        cMovementControl = GetComponent<MovementController>();
        cModifiers = GetComponent<CharModifiers>();
        cAnimator = GetComponent<Animator>();
        cCombat = GetComponent<Combat>();
        cTeam = GetComponent<Team>();

        startPosition = transform.position;
        PreProcessSkills();
    }

    // Update is called once per frame
    void Update()
    {


        if (State == aiState.WaitForEnemy)
        {
            if (attemptToGetTargetTimer.Check())
            {
                if (AttemptToGetTarget())
                {
                    State = aiState.Fight;
                    cCombat.SetCombatState(true);
                }
            }
            else if (holdPosition)
                AttemptToMove(startPosition, 1);
        }
        if (State == aiState.Fight)
        {
            if (TargetIsDead())
            {
                if (!AttemptToGetTarget())
                {
                    State = aiState.WaitForEnemy;
                    cCombat.SetCombatState(false);
                }
            }
            else
            {
                //Debug.Log(Vector3.Distance(transform.position, target.transform.position));
                //transform.LookAt(target);
                var distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
                if (combatState == CombatState.Chasing)
                {
                    var fightDistance = cCombat.GetFightDistance(false);
                    if(distanceToTarget > cCombat.GetFightDistance(true) || !IsTargetVisible())
                        AttemptToMove(target.transform.position, fightDistance);
                    else
                    {
                        cMovementControl.CancelMovement();
                        combatState = CombatState.Attack;
                    }
                }
                if (combatState == CombatState.Attack)
                {
                    if((distanceToTarget > cCombat.GetFightDistance(true) || !IsTargetVisible()) && cCombat.CanAttackWhileMoving())
                        combatState = CombatState.Chasing;
                    else if (!cModifiers.IsModifierActive(ModifierType.DisableAttack))
                        cCombat.Attack();
                }
                    
            }

            if (cCastControl.CastIsCharging())
            {
                if (!cCastControl.CanMoveWhileCharging())
                    cMovementControl.CancelMovement();
            }
            else
                AttemptToUseSkills();

        }

    }

    bool IsTargetVisible()
    {
        var layerMask = LayerMask.GetMask("Ignore Raycast");
        return (!Physics.Linecast(transform.position + Vector3.up, target.transform.position + Vector3.up, layerMask));
    }
    bool AttemptToGetTarget()
    {
        target = cTeam.GetClosestTeamMember(cTeam.GetOppositeTeam(), GetTargetCheckPosition(), aggroRadius);
        if (target != null)
            return true;
        else
            return false;
    }

    void AttemptToMove(Vector3 target_pos, float distanceToKeep)
    {
        if (cModifiers.IsModifierActive(ModifierType.DisableMovement))
            cMovementControl.CancelMovement();
        else
            cMovementControl.MoveToV3(target_pos, distanceToKeep);
    }

    bool SetPlayerAsTarget()
    {
        PlayerController player = (PlayerController)FindObjectOfType(typeof(PlayerController));
        if (player!=null)
        {
            target = player.GetComponent<Transform>();
            return true;
        }
        else
            return false;
    }

    public Transform GetTarget()
    {
        return target;
    }

    public aiState GetState()
    {
        return State;
    }
    bool TargetIsDead()
    {
        if (target == null)
            return true;

        return target.GetComponent<Health>().IsDead();
    }

    Vector3 GetTargetCheckPosition()
    {
        if (holdPosition)
            return startPosition;
        else
            return transform.position;
    }

    private void AttemptToUseSkills()
    {
        
        foreach (AISkill skill  in skills)
            if (skill.ReadyToUse(this))
                cCastControl.CastStart(skill.cast);
    }
    private void PreProcessSkills()
    {
        foreach (AISkill skill  in skills)
        {
            skill.chanceTimer.SetActive(true);

            if(skill.condition != null && skill.condition.GetSpecificCondition().name != "")
                specificConditions.Add(skill.condition.GetSpecificCondition().name, skill.condition.GetSpecificCondition());
        }
    }
    public void SpecificConditionsSetActive(string name, bool active)
    {
        if (specificConditions.ContainsKey(name))
            specificConditions[name].active = active;
    }
    public Dictionary<string,CastSpecificCondition> SpecificConditionsGetDictionary()
    {
        return specificConditions;
    }

    private void OnDrawGizmos() 
    {
        /*var player= GameObject.FindObjectOfType<PlayerController>();
        var myPos = transform.position;
        var playerPos = player.transform.position;
        var angle = Utility.VectorDirection(transform, player.transform);
        var point = Utility.PointInDirection(myPos, angle, 5f);
        Gizmos.DrawSphere(point,1f);
        Handles.Label(transform.position+Vector3.up*3, angle.ToString());*/
    }
    // STRANGE ANIMATION EVENTS
    void AnimalSound(){}
    void Walking(){}
    void Eating(){}
}
