﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentKnockback : MonoBehaviour
{
    public Vector3 pos;
    float minStep = 0.02f;
    float speedDiv = 5;
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var dist = Vector3.Distance(pos, transform.position);
        //Debug.Log(dist);
        if (dist > 0.2f)
        {
            var speed = Mathf.Min(Mathf.Max(dist / speedDiv, minStep) * Time.deltaTime * 50, dist);
            pos = Vector3.MoveTowards(pos, transform.position, speed);
            transform.position = Vector3.MoveTowards(transform.position, pos, speed);
        }
        else
            pos = transform.position;

    }
    private void OnDrawGizmos() 
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawSphere(pos + Vector3.up, 0.3f);
    }
}
