﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType
{
    MeleeSwing, BowShot, HitSound, Death
}
public class AudioController : MonoBehaviour
{
    [SerializeField] SoundPreset mainPreset = null;
    [SerializeField] SoundPreset[] presetOverrides = null;
    AudioSource source;
    SoundPreset soundPreset;
    void Start()
    {
        source = GetComponent<AudioSource>();
        soundPreset = Instantiate(mainPreset);
        UsePresetOverrides();
    }

    private void UsePresetOverrides()
    {
        foreach (var preset in presetOverrides)
        {
            foreach (var list in preset.lists)
            {
                foreach (var main_list in soundPreset.lists)
                {
                    if (list.name == main_list.name)
                    {
                        main_list.sounds.Clear();
                        main_list.sounds = list.sounds;
                    }
                }
            }
        }
    }

    public void PlaySound(SoundType soundType)
    {
        soundPreset.PlaySoundFromList(soundType, source);
    }
    public void PlaySound(SoundType soundType, float delay)
    {
        soundPreset.PlaySoundFromList(soundType, source, delay);
    }


}
