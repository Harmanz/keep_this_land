﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Modifier
{
    public ModifierType type;
    public float value;
    public float default_value = 1f;
    public bool binaryEffect = false;
    bool compare_only_duration = false;
    Dictionary<string, float> values = new Dictionary<string, float>();
    Dictionary<string, float> durations = new Dictionary<string, float>();
    public Modifier(ModifierType type, float def_value, bool binary)
    {
        if (def_value == 0 && !binary)
            Debug.LogError("MODIFIER CANT BE NON BINARY WITH DEFAULT VALUE ZERO");
        this.type = type;
        value = def_value;
        default_value = def_value;
        binaryEffect = binary;
    }
    public void Process()
    {   
        /*var str = type.ToString() + " "+values.Count.ToString();
        foreach( var key in values.Keys)
            str += key.ToString() + " ";
        Debug.Log(str);*/
        if (values.Count == 0)
        {
            value = default_value;
            return;
        }
        
        var positive = 1f;
        var negative = 1f;
        var values_copy = new Dictionary<string, float>(values);
        foreach(var effect in values_copy.Keys)
        {
            if(values[effect] > 1)
                positive += values[effect] - 1;
            else
                negative *= values[effect];

            if (durations[effect] > 0)
                durations[effect] -= Time.deltaTime;
            else if (durations[effect] != -1)
            {
                durations.Remove(effect);
                values.Remove(effect);
            }
        }
        if (binaryEffect)
            value = default_value == 0 ? 1 : 0;
        else
            value = default_value * positive * negative;
    }
    public void Set(string id, float new_value, float new_duration)
    {
        if(!values.ContainsKey(id))
        {
            //Debug.Log(id+" "+new_duration.ToString());
            values.Add(id, new_value);
            durations.Add(id, new_duration);
        }
        else if (new_duration > durations[id]  || (new_value > values[id]  && !compare_only_duration))
        {
            durations[id]  = new_duration;
            values[id]  = new_value;
        }
    }
}

public class CharModifiers : MonoBehaviour
{
    [SerializeField] DamageElement immuneToElement = DamageElement.None;
    [SerializeField] List<ModifierType> immunityList = new List<ModifierType>();
    Dictionary<ModifierType, Modifier> Modifiers = new Dictionary<ModifierType, Modifier>();
    Animator cAnimator;
    MovementController cMovementControl;

    // Start is called before the first frame update
    void Start()
    {
        cAnimator = GetComponent<Animator>();
        cMovementControl = GetComponent<MovementController>();

        Modifiers.Add(ModifierType.DamageIncome,   new Modifier(ModifierType.DamageIncome, 1, false));
        Modifiers.Add(ModifierType.DamageOutcome,  new Modifier(ModifierType.DamageOutcome, 1, false));
        Modifiers.Add(ModifierType.Speed,          new Modifier(ModifierType.Speed, 1, false));
        Modifiers.Add(ModifierType.AngularSpeed,   new Modifier(ModifierType.AngularSpeed, 1, false));
        Modifiers.Add(ModifierType.DisableAttack,  new Modifier(ModifierType.DisableAttack, 0, true));
        Modifiers.Add(ModifierType.DisableCast,    new Modifier(ModifierType.DisableCast, 0, true));
        Modifiers.Add(ModifierType.DisableMovement,new Modifier(ModifierType.DisableMovement, 0, true));
        Modifiers.Add(ModifierType.DisableAnim,    new Modifier(ModifierType.DisableAnim, 0, true));
    }

    // Update is called once per frame
    void Update()
    {
        foreach( Modifier mod in Modifiers.Values)
        {
            mod.Process();
        }
        CheckDisables();
    }

    public DamageElement GetElementImmunity()
    {
        return immuneToElement;
    }
    
    void CheckDisables()
    {
        if (IsModifierActive(ModifierType.DisableMovement))
            if(cMovementControl.enabled)
                cMovementControl.CancelMovement();

        if(IsModifierActive(ModifierType.DisableMovement) && IsModifierActive(ModifierType.DisableAttack) && IsModifierActive(ModifierType.DisableCast))
        {   
            if (!cAnimator.GetBool("stunned"))
            {
                cAnimator.SetBool("stunned", true);
                cAnimator.SetTrigger("stun");
            }   
        }
        else
            cAnimator.SetBool("stunned", false);

        if (IsModifierActive(ModifierType.DisableAnim))
            cAnimator.enabled = false;
        else
            cAnimator.enabled = true;
    }

    public void SetValue(string id, ModifierType mod_type, float value, float duration)
    {
        //Debug.Log(mod_type);
        if (!immunityList.Contains(mod_type))
            Modifiers[mod_type].Set(id, value, duration);
    }

    public float GetValue(ModifierType mod_type)
    {
        if (!Modifiers.ContainsKey(mod_type))
        {
            Debug.Log("CharModifiers.cs : There is no such modifier in modifiers list! Add it manualy!");
            return 0;
        }
        return Modifiers[mod_type].value;
    }
    public bool IsModifierActive (ModifierType mod_type)
    {
        if (!Modifiers.ContainsKey(mod_type))
        {
            Debug.Log("CharModifiers.cs : There is no such modifier in modifiers list! Add it manualy!");
            return false;
        }
        float modifier_value = Modifiers[mod_type].value;
        float default_value = Modifiers[mod_type].default_value;
        if (modifier_value != default_value)
            return true;
        else
            return false;
    }
}
