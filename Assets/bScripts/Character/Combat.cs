﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Combat : MonoBehaviour
{
    [SerializeField] weaponConfig rightHandWeapon = null;
    [SerializeField] weaponConfig leftHandWeapon = null;
    Transform leftHand = null;
    Transform rightHand = null;
    bool inCombat = false;
    float damage = 10f;
    float predictChance = 0;
    float fightDistance = 1.5f;
    float additionalHitDistance = 0.3f;
    bool isRanged = false;
    public Cooldown attackCooldown = new Cooldown(1.5f);
    bool canAttackWhileMoving = true;
    WeaponType weaponType = WeaponType.OneHanded;
    Dictionary<WeaponType,int> hitAnimCount = new Dictionary<WeaponType, int>();
    Transform target;
    Animator cAnimator = null;
    AIController cController = null;
    AudioController cAudio = null;

    // Start is called before the first frame update
    void Start()
    {
        attackCooldown.SetRandomCooldown();
        SetHitAnimCounts();
        
        cAnimator = GetComponent<Animator>();
        cController = GetComponent<AIController>();
        cAudio = GetComponent<AudioController>();

        rightHand = FindHand("R");
        leftHand = FindHand("L");

        if (leftHandWeapon != null)
            leftHandWeapon.Equip(leftHand, this);
        if (rightHandWeapon != null)
            rightHandWeapon.Equip(rightHand, this);
        else
            Debug.LogError(transform.name+" Combat: Right hand must contain any weapon"); // even archers should have melee weapon in right hand to switch to in in melee
    }

    // Update is called once per frame
    void Update()
    {
        cAnimator.SetBool("inCombat", inCombat);
        attackCooldown.ProcessWithoutCheck();
        target = cController.GetTarget();
    }

    public void Attack()
    {
        if(target != null)
        {  
            var posToFollow = target.position;
            posToFollow.y = transform.position.y;
            var rotate_speed = GetComponent<NavMeshAgent>().angularSpeed/1000f;
            var newDir = Vector3.RotateTowards(transform.forward, posToFollow - transform.position, rotate_speed * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(newDir);
            var currentDir = Vector3.Angle(transform.forward, posToFollow - transform.position);
            if (currentDir < 20 && attackCooldown.Check(0))
            {
                cAnimator.SetInteger("attackAnimNum", GetRandomHitAnim(weaponType));
                cAnimator.SetTrigger("attack");
            }
        }
    }

    public bool AttackInProgress()
    {
        return !(cAnimator.GetInteger("attackAnimNum") == -1);
    }
    public bool ReadyToAttack()
    {
        return attackCooldown.IsReady();
    }
    void Swing()
    {
        if (!isRanged)
            cAudio.PlaySound(SoundType.MeleeSwing);
        else
        {
            cAnimator.ResetTrigger("attack");
            cAnimator.SetInteger("attackAnimNum", -1);
            if(leftHandWeapon.projectilePrefab)
            {
                cAudio.PlaySound(SoundType.BowShot);
                GameObject projectile = Instantiate(leftHandWeapon.projectilePrefab, leftHand.position, Quaternion.identity);
                projectile.GetComponent<Projectile>().Initialize(transform, target, damage, predictChance, GetComponent<Team>().GetOppositeTeam());
            }
            if(leftHandWeapon.spell)
            {
                leftHandWeapon.spell.CastSpell(GetComponent<Team>().GetTeam(), leftHandWeapon.spell.GetChargeTime(), transform, target, 1);
            }
        }
    }
    void Hit()
    {
        cAnimator.ResetTrigger("attack");
        cAnimator.SetInteger("attackAnimNum", -1);
        if (!isRanged)
        {   
            if(target != null)
            {
                var dist = Vector3.Distance(transform.position, target.transform.position);
                if(dist < (fightDistance+1.5f))
                    target.GetComponent<Health>().GetDamage(damage, transform, true, DamageType.Melee, DamageElement.Physics);
            }
        }
    }

    public void SetCombatState(bool combatState)
    {
        inCombat = combatState;
    }

    public void SetStats(float damage, float fightDistance, float attackCooldown, bool attackWhileMoving, WeaponType weaponType, bool isRanged, float predictChance)
    {
        this.damage = damage;
        this.fightDistance = fightDistance;
        this.weaponType = weaponType;
        this.isRanged = isRanged;
        this.attackCooldown.cd = attackCooldown;
        this.canAttackWhileMoving = attackWhileMoving;
        this.predictChance = predictChance;
        cAnimator.SetInteger("wpnType", (int)weaponType);
    }

    public bool CanAttackWhileMoving()
    {
        return canAttackWhileMoving || (cAnimator.GetInteger("attackAnimNum") == -1 && attackCooldown.IsReady());
    }
    Transform FindHand(string R_or_L)
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            Transform charTransform = transform.GetChild(i);
            string path = "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_"+R_or_L+"/Shoulder_"+R_or_L+"/Elbow_"+R_or_L+"/Hand_"+R_or_L;
            Transform hand = charTransform.Find(path);
            if(hand != null)
                return hand;
        }
        return null;
    }

    void SetHitAnimCounts()
    {
        hitAnimCount.Add(WeaponType.Unarmed, 8);
        hitAnimCount.Add(WeaponType.OneHanded, 7);
        hitAnimCount.Add(WeaponType.TwoHanded, 6);
        hitAnimCount.Add(WeaponType.Bow, 6);
        hitAnimCount.Add(WeaponType.Cast, 1);
        hitAnimCount.Add(WeaponType.Giant, 6);
        hitAnimCount.Add(WeaponType.Spear, 5);
        hitAnimCount.Add(WeaponType.Dual, 8);
    }
    int GetRandomHitAnim(WeaponType weaponType)
    {
        int animCount = hitAnimCount[weaponType];
        return Random.Range(0,animCount);
    }
   
    public float GetFightDistance(bool getMax)
    {
        if(getMax)
            return (fightDistance + additionalHitDistance);
        else
            return(fightDistance);
    }

    private void OnDrawGizmos() 
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(transform.position, fightDistance);
    }

}
