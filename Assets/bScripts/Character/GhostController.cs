﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostController : MonoBehaviour
{
    [SerializeField] GameObject materialGhostPrefab = null;
    NavMeshAgent cAgent = null;
    Animator cAnimator = null;
    Vector3 PointToMove;
    Vector3 CenterPoint;
    float randomMin = 5f;
    float randomMax = 10f;
    float pointChangeDistance = 3f;
    Cooldown materializeCooldown = new Cooldown(10f);
    // Start is called before the first frame update
    void Start()
    {
        materializeCooldown.SetTime(Utility.Choose(new float[] { 3, 4, 5, 6, 7, 8 }));
        StartCoroutine(GetHealthFromMaterial());
        cAgent = GetComponent<NavMeshAgent>();
        cAnimator = GetComponentInChildren<Animator>();
        CenterPoint = GameObject.FindGameObjectWithTag("RoomCenter").transform.position;
        MoveToRandomPoint();
    }

    // Update is called once per frame
    void Update()
    {
        CheckDistanceToPoint();
        CheckForMaterializing();
    }

    IEnumerator GetHealthFromMaterial()
    {
        yield return new WaitForEndOfFrame();
        var magic = GetComponentInChildren<Magic>();
        if (magic)
        {
            materializeCooldown.SetTimeToMax();
            var caster = magic.GetCaster();
            GetComponent<Health>().SetHealth(caster.GetComponent<Health>().currentHealth);
            GameObject.DestroyImmediate(caster.gameObject);
        }
    }

    private void CheckForMaterializing()
    {
        if (materializeCooldown.Check())
        {
            
            StartCoroutine(WaitForMaterialGhostHealthLoaded());
            
        }
    }
    IEnumerator WaitForMaterialGhostHealthLoaded()
    {
        var materialGhost = Instantiate(materialGhostPrefab, transform.position, transform.rotation);
        yield return new WaitForEndOfFrame();
        materialGhost.GetComponent<Health>().SetHealth(GetComponent<Health>().currentHealth);
        GameObject.Destroy(this.gameObject);
    }
    private void CheckDistanceToPoint()
    {
        var distance = cAgent.remainingDistance;
        if (distance < pointChangeDistance)
            MoveToRandomPoint();
    }

    private void MoveToRandomPoint()
    {
        PointToMove = GetPointToMove();
        MoveToV3(PointToMove, 0);
        cAnimator.SetTrigger("playIdle");
    }

    private Vector3 GetPointToMove()
    {
        var rx = Random.Range(randomMin,randomMax) * Utility.RandomSign();
        var rz = Random.Range(randomMin,randomMax) * Utility.RandomSign();
        return new Vector3(CenterPoint.x + rx, transform.position.y, CenterPoint.z + rz);
    }

    public void MoveToV3(Vector3 destination, float distanceToKeep)
    {
        cAgent.stoppingDistance = 0;
        cAgent.destination = destination;
        cAgent.isStopped = false;
    }
}
