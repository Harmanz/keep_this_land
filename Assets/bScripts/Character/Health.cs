﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Health : MonoBehaviour
{
    [SerializeField] float maxHealth = 100;
    [SerializeField] bool boss = false;
    [SerializeField] uiMiniHealthBarController healthBar = null;
    public float currentHealth;
    public bool invulnerable = false;
    public bool canDie = true;
    bool isDead = false;
    float painAmount = 0f;
    Cooldown recentDamageCooldown = new Cooldown(10f);
    Animator cAnimator = null;
    AudioController cAudio = null;
    PlayerStats cPlayerStats = null;
    CharModifiers cModifiers = null;
    eTeam team = eTeam.EnemyGeneric;
    void Start()
    {
        SetMyTeam();
        recentDamageCooldown.timer = 0;
        currentHealth = maxHealth;

        cPlayerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        cAnimator = GetComponent<Animator>();
        cAudio = GetComponent<AudioController>();
        cModifiers = GetComponent<CharModifiers>();

        if (boss)
        {
            var mainUI = GameObject.FindGameObjectWithTag("UI");
            mainUI.GetComponent<uiMainScreen>().SwitchBossBar(this);
        }
        
    }

    private void Update()
    {
        recentDamageCooldown.ProcessWithoutCheck();
        ProcessWoundAnim();
    }

    private void ProcessWoundAnim()
    {
        if (cAnimator)
        {
            if (painAmount > 0)
            {
                painAmount -= Time.deltaTime;
                cAnimator.SetFloat("painAmount", painAmount);
            }
            else
                cAnimator.SetFloat("painAmount", 0);
        }
    }

    public float TimeSinceDamage()
    {
        var value = recentDamageCooldown.cd - recentDamageCooldown.GetTime();
        if(value < recentDamageCooldown.cd)
            return value;
        else
            return -1;
    }

    public void GetDamage(float value, Transform source, bool playStandartSound, DamageType damageType, DamageElement element)
    {
        if (invulnerable || isDead)
            return;

        recentDamageCooldown.SetTimeToMax();

        TurnOnWoundAnim();
        PlayStandartSound(playStandartSound);

        //value = Utility.GetRandomValue(CalculateDamage(value), 0.3f, true);
        value = CalculateDamage(value, damageType, element, source);
        cPlayerStats.EnergyRefillByDamage(value, team);
        currentHealth = Mathf.Max(currentHealth - value, 0);

        if (healthBar)
            healthBar.SetHealth(currentHealth / maxHealth);

        if (currentHealth <= 0 && canDie)
            Die();
    }

    private void CreateFloaingText(float value, bool is_crit)
    {
        if (value == 0)
            return;
        var floatingTextPrefab = (GameObject)Resources.Load("ui_floating_text");
        var newText = Instantiate(floatingTextPrefab, transform.position + Utility.GetRandomVector(new Vector3(1, 0.5f, 0)), transform.rotation);
        newText.GetComponent<uiFloatingText>().Initialize(value, is_crit);
    }

    private float CalculateDamage(float value, DamageType damageType, DamageElement element, Transform source)
    {
        var newValue = value;
        var healthPercent = currentHealth / maxHealth;
        var mod = 1f;
        var isCrit = false;
        if (cModifiers && cModifiers.IsModifierActive(ModifierType.DamageIncome))
            mod = cModifiers.GetValue(ModifierType.DamageIncome);

        if (team == eTeam.Player)
        {
            if (Utility.Chance(cPlayerStats.stats["ZeroDamageChanceAdd"]))
                newValue = 0;
            else
            {
                cPlayerStats.EventCastCall(ItemCastEvent.PlayerHit, source);
                newValue += DamageBonus(value, "DamageIncome");
                newValue = cPlayerStats.ShieldDamage(newValue * mod);
            }
        }
        else
        {
            newValue = value + value * cPlayerStats.stats["DamageOutcomeMultBase"];
            newValue += DamageBonus(value, element.ToString()+"Damage");
            if (damageType != DamageType.Familiar)
            {
                cPlayerStats.EventCastCall(ItemCastEvent.EnemyHit, transform);
                newValue += DamageBonus(value, "Damage");
                if (healthPercent > 0.8)
                    newValue += DamageBonus(value, "DamageToHealthy");
                if (healthPercent < 0.3)
                    newValue += DamageBonus(value, "DamageToWounded");
            
                var critChance = cPlayerStats.stats["CritChanceBase"] + cPlayerStats.stats["CritChanceAdd"] + (cPlayerStats.stats["CritChanceBase"] * cPlayerStats.stats["CritChanceMult"]);
                if (Utility.Chance(critChance))
                {
                    isCrit = true;
                    cPlayerStats.EventCastCall(ItemCastEvent.Crit, transform);
                    newValue += DamageBonus(newValue, cPlayerStats.stats["CritMultBase"] + cPlayerStats.stats["CritDamageMult"], cPlayerStats.stats["CritDamageAdd"] );
                }
                newValue *= mod;
                var vampirism = DamageBonus(newValue, "AttackVampirism");
                if (vampirism > 0)
                    cPlayerStats.GetComponent<Health>().GetHealing(vampirism);
            }
        }
        newValue = Mathf.Round(newValue);
        CreateFloaingText(-newValue, isCrit);
        return newValue;
    }

    private float DamageBonus(float baseDamage, string key)
    {
        var modifiedDamage = (baseDamage * cPlayerStats.stats[key+"Mult"]) + cPlayerStats.stats[key+"Add"];
        return modifiedDamage;
    }

    private float DamageBonus(float baseDamage, float mult, float add)
    {
        var modifiedDamage = (baseDamage * mult) + add;
        return modifiedDamage;
    }

    private void PlayStandartSound(bool playStandartSound)
    {
        if (cAudio && playStandartSound)
            cAudio.PlaySound(SoundType.HitSound);
    }

    private void TurnOnWoundAnim()
    {
        if (cAnimator)
        {
            painAmount = 1.0f;
            cAnimator.SetFloat("woundInMovementFloat", Random.Range(0f, 4f));
            cAnimator.SetInteger("woundAnimNum", Random.Range(0, 5));
        }
    }

    public float GetHealthPercent()
    {
        return (currentHealth/maxHealth);
    }
    public void GetHealing(float value)
    {
        CreateFloaingText(value, false);
        currentHealth = Mathf.Min(currentHealth + value, maxHealth);
        if (healthBar)
            healthBar.SetHealth(currentHealth/maxHealth);
    }
    public void HealToMax()
    {
        currentHealth = maxHealth;
    }
    public void SetInvulnerability(bool invulnerable)
    {
        this.invulnerable = invulnerable;
    }
    public void SetHealth(float value)
    {
        currentHealth = Mathf.Clamp(value, 0, maxHealth);
        if (healthBar)
            healthBar.SetHealth(currentHealth/maxHealth);
    }

    public void Die()
    {
        if (team == eTeam.Player)
            cPlayerStats.EventCastCall(ItemCastEvent.Death, transform);
        else
            cPlayerStats.EventCastCall(ItemCastEvent.Kill, cPlayerStats.transform);
            
        if (currentHealth > 0)
            return;

        currentHealth = 0;
        isDead = true;
        if (cAnimator)
        {
            cAnimator.SetInteger("deathAnimNum",Random.Range(0,4));
            cAnimator.SetBool("dead", true);
            cAnimator.SetTrigger("death");
        }
        if (healthBar)
            healthBar.gameObject.SetActive(false);
        cAudio.PlaySound(SoundType.Death);
        var aiController = GetComponent<AIController>();
        var agent = GetComponent<NavMeshAgent>();
        var collider = GetComponent<CapsuleCollider>();
        var cteam = GetComponent<Team>();
        var movementControl = GetComponent<MovementController>();
        var castControl = GetComponent<CastController>();
        if (aiController)
            aiController.enabled = false;
        if (cteam)
            cteam.enabled = false;
        if (collider)
            collider.enabled = false;
        if (agent)
            agent.enabled = false;
        if (movementControl)
            movementControl.enabled = false;
        if (castControl)
            castControl.CastStopCharging();
        foreach(var particle in GetComponentsInChildren<ParticleSystem>())
            particle.Stop();

        Destroy(this.gameObject, 10f);
    }

    public void SetMaxHealth(float value)
    {
        maxHealth = value;
        SetHealth(value);
    }
    public bool IsDead()
    {
        return isDead;
    }

    private void SetMyTeam()
    {
        if (gameObject.GetComponent<Team>())
            team = gameObject.GetComponent<Team>().GetTeam();
        else
            team = eTeam.EnemyGeneric;
    }

}

public enum DamageType {Spell, EnergySpell, Melee, Projectile, Familiar}
public enum DamageElement {Arcane, Frost, Fire, Earth, Air, Physics, Familiar, None}