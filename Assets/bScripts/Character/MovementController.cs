﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    [SerializeField] bool updateAnimator = true;
    [SerializeField] float acceleration = 0.05f;
    NavMeshAgent cAgent;
    CharModifiers cModifiers = null;
    [SerializeField] float minSpeed = 0f;
    [SerializeField] float maxSpeed = 5f;

    float currentSpeed;
    float normalAngSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        cModifiers = GetComponent<CharModifiers>();
        cAgent = transform.GetComponent<NavMeshAgent>();
        currentSpeed = minSpeed;
        cAgent.speed = minSpeed;
        normalAngSpeed = cAgent.angularSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        CheckModifiers();
        ProcessSpeed();
        if (updateAnimator)
            UpdateAnimator();
    }

    void CheckModifiers()
    {
        if (cModifiers && cModifiers.IsModifierActive(ModifierType.Speed))
        {
            var modifier = cModifiers.GetValue(ModifierType.Speed);
            currentSpeed = maxSpeed * modifier;
            cAgent.speed = currentSpeed;
        }
        else
            currentSpeed = maxSpeed;
        if (cModifiers && cModifiers.IsModifierActive(ModifierType.AngularSpeed))
        {
            var modifier = cModifiers.GetValue(ModifierType.AngularSpeed);
            cAgent.angularSpeed = normalAngSpeed * modifier;
        }
        else
            cAgent.angularSpeed = normalAngSpeed;
    }

    void ProcessSpeed()
    {
        cAgent.speed =  Utility.ChangeValueWithSpeed(cAgent.speed, currentSpeed, acceleration * Time.deltaTime);
    }

    public void MoveToV3(Vector3 destination, float distanceToKeep)
    {
        cAgent.stoppingDistance = 0;
        cAgent.destination = destination;
        cAgent.isStopped = false;
        
    }

    public void CancelMovement()
    {
        cAgent.isStopped = true;
        cAgent.speed = minSpeed;
        cAgent.velocity = Vector3.zero; 
    }

    public static void MoveToV3(NavMeshAgent agent, Vector3 destination, float distanceToKeep)
    {
        agent.stoppingDistance = distanceToKeep;
        agent.destination = destination;
        agent.isStopped = false;
    }

    private void UpdateAnimator()
    {
        
        Vector3 velocity = cAgent.velocity;
        Vector3 localVelocity = transform.InverseTransformDirection(velocity);
        
        float speed = localVelocity.z;
        GetComponent<Animator>().SetFloat("forwardSpeed", speed);
    }

    public void SetSpeed(float minSpeed, float maxSpeed)
    {
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
    }
    public void SetSpeed(float speed)
    {
        this.minSpeed = speed;
        this.maxSpeed = speed;
    }
    void FootR() {} void FootL() {}
}
