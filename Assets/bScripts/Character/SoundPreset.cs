﻿using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


[System.Serializable]
public class SoundsList
{
    public SoundType name;
    public List<AudioClip> sounds;

    public SoundsList()
    {

    }
}

[CreateAssetMenu(fileName = "Sound preset", menuName = "Audio/Make new sound preset", order = 1)]
public class SoundPreset : ScriptableObject 
{
    [SerializeField] public SoundsList[] lists;

    public void PlaySoundFromList(SoundType name, AudioSource source)
    {
        source.clip = GetRandomClipFromList(name);
        source.Play();
    }

    public void PlaySoundFromList(SoundType name, AudioSource source, float delay)
    {
        source.clip = GetRandomClipFromList(name);
        source.PlayDelayed(delay);
    }

    private AudioClip GetRandomClipFromList(SoundType name)
    {
        foreach (var list in lists)
            if (list.name == name)
                return list.sounds[Random.Range(0,list.sounds.Count)];
        Debug.LogError("There is no such sound list in preset: "+name);
        return null;
    }
}




