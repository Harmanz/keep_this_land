﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eTeam
{
    Player,EnemyGeneric,Environment
}
public class Team : MonoBehaviour
{
    [SerializeField] eTeam team = eTeam.EnemyGeneric;
    public bool canBeTarget = true;

    public void SetTeam(eTeam team)
    {
        this.team = team;
    }

    public eTeam GetTeam()
    {
        return team;
    }
    public eTeam GetOppositeTeam()
    {
        if (team == eTeam.EnemyGeneric)
            return eTeam.Player;
        else
            return eTeam.EnemyGeneric;
    }
    public void SwapTeam()
    {
        team = GetOppositeTeam();
    }

    public List<Transform> GetTeamMembers(eTeam requiredTeam)
    {
        List<Transform> members = new List<Transform>();
        foreach (Team entity in GameObject.FindObjectsOfType<Team>())
        {
            var entityHealth = entity.GetComponent<Health>();
            if (entity.enabled && entityHealth!=null && entity.GetTeam() == requiredTeam)
                members.Add(entity.transform);
        }
        return members;
    }

    public Transform GetClosestTeamMember(eTeam requiredTeam, Vector3 position, float maxDistance)
    {
        List<Transform> members = GetTeamMembers(requiredTeam);
        if (members.Count == 0)
            return null;

        Transform chosenMember = null;
        float chosenMemberDistance = Mathf.Infinity;
        foreach (Transform member in members)
        {
            if (!member.GetComponent<Team>().canBeTarget)
                continue;
            float distanceToMember = Vector3.Distance(position, member.position);
            if (distanceToMember > maxDistance)
                continue;
            if (distanceToMember < chosenMemberDistance)
            {
                chosenMember = member;
                chosenMemberDistance = distanceToMember;
            }
        }
        return chosenMember;
    }
    public Transform GetRandomTeamMember(eTeam requiredTeam)
    {
        List<Transform> members = GetTeamMembers(requiredTeam);
        if (members.Count == 0)
            return null;

        
        return members[Random.Range(0, members.Count-1)];
    }
}


