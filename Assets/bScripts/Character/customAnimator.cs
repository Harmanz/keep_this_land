﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum customAnimation
{
    combatIdle1,combatIdle2,combatIdle3,combatIdle4,combatIdle5,combatIdle6,
    idle1,idle2
}
public class customAnimator : MonoBehaviour
{
    [SerializeField] new customAnimation animation = customAnimation.combatIdle1;
    [SerializeField] float animSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        var cAnimator = GetComponent<Animator>();
        cAnimator.speed = animSpeed;
        cAnimator.SetInteger("customAnimNum",(int) animation );
        cAnimator.SetTrigger("resetAnim");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
