﻿using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum WeaponType
{
    Unarmed,OneHanded,TwoHanded,Bow,Cast,Giant,Dual,Spear
}

[CreateAssetMenu(fileName = "Weapon", menuName = "Characters/Make new weapon config", order = 1)]
public class weaponConfig : ScriptableObject 
{
    [SerializeField] GameObject weaponPrefab = null;
    [SerializeField] bool hasStats = true;
    [ConditionalField("hasStats", false, true)] public float damage = 35f;
    [ConditionalField("hasStats", false, true)] public float fightDistance = 1f;
    [ConditionalField("hasStats", false, true)] public float attackCooldown = 1.5f;
    [ConditionalField("hasStats", false, true)] public bool canAttackWhileMove = true;
    [ConditionalField("hasStats", false, true)] public WeaponType weaponType = WeaponType.OneHanded;
    [ConditionalField("hasStats", false, true)] public bool ranged = false;
    [ConditionalField("ranged", false, true)] public GameObject projectilePrefab = null;
    [ConditionalField("ranged", false, true)] public Cast spell = null;
    [ConditionalField("ranged", false, true)] public float predictChance;
    
    public void Equip(Transform hand, Combat combat)
    {
        if (weaponPrefab != null && hand != null)
            Instantiate(weaponPrefab, hand);
        Use(combat);
    }
    public void Use(Combat combat)
    {
        if (hasStats)
            combat.SetStats(damage, fightDistance, attackCooldown, canAttackWhileMove, weaponType, IsRanged(), predictChance);
    }

    public bool IsRanged()
    {
        return ranged;
    }
}




