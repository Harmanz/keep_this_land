﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPoint : MonoBehaviour
{
    [SerializeField] Material activeMat = null;
    [SerializeField] Material inactiveMat = null;
    [SerializeField] ParticleSystem activeParticle = null;
    // Start is called before the first frame update
    void Start()
    {
        activeParticle.Stop(true);
        GetComponentInChildren<MeshRenderer>().material = inactiveMat;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
