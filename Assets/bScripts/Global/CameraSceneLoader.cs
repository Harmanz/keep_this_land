﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TigerForge;

public class CameraSceneLoader : MonoBehaviour
{
    [SerializeField] Image blackScreen = null;
    bool fadeIn = true;
    void Start()
    {
        blackScreen.color = Utility.ChangeColorAlpha(blackScreen.color, 1);
        EventManager.StartListening("SCENE_LOAD_START", OnSceneLoadStart);
    }
        
    void Update()
    {
        if (fadeIn && blackScreen.color.a > 0)
            blackScreen.color = Utility.ChangeColorAlpha(blackScreen.color, -Time.deltaTime);
    }
    IEnumerator LoadSceneCoroutine(float time, string sceneName)
    {
        while (blackScreen.color.a < 1)
        {
            blackScreen.color = Utility.ChangeColorAlpha(blackScreen.color, Time.deltaTime);
            yield return null;
        }
        fadeIn = true;
        SceneManager.LoadScene(sceneName);
    }

    void OnSceneLoadStart()
    {
        var sceneName = EventManager.GetString("SCENE_NAME");
        fadeIn = false;
        StartCoroutine(LoadSceneCoroutine(1, sceneName));
    }
}
