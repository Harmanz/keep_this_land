﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Leguar.TotalJSON;
using UnityEngine;
using TigerForge;

public enum FileType { Progression, Localization, Config}
public class FilesHandler : MonoBehaviour
{
    string profile = "Profile1";
    string localizationFile = "en";
    string progressionFileBase = "progression_base";
    string configFile = "config";

    public Dictionary<FileType, JSON> files = new Dictionary<FileType, JSON>();

    void Start()
    {

    }

    private void Awake() 
    {
        QualitySettings.SetQualityLevel(10, true);
        LoadJSFromFile(FileType.Localization);
        LoadJSFromFile(FileType.Progression);
        LoadJSFromFile(FileType.Config);
    }
    private void LoadJSFromFile(FileType type)
    {
        var text = "";
        if (type == FileType.Localization)
            text = Resources.Load("texts_" + localizationFile).ToString();
        if (type == FileType.Progression)
        {
            var profileProgressionPath = Application.persistentDataPath + "/"+profile+"/progression.json";
            
            if (System.IO.File.Exists(profileProgressionPath))
            {
                var reader = new StreamReader(profileProgressionPath);
                text = reader.ReadToEnd();
            }
            else
               text = Resources.Load(progressionFileBase).ToString();
        }
        if (type == FileType.Config)
            text = Resources.Load(configFile).ToString();

        if (!files.ContainsKey(type))
            files.Add(type, JSON.ParseString(text));
        else
        {
            files[type].Clear();
            files[type] = JSON.ParseString(text);
        }
    }

    public string GetText(FileType type, string key)
    {
        if (files[type].ContainsKey(key))
            return files[type].GetString(key);
        else
        {
            files[type].Add(key, key);
            return (key);
        }
    }

    public string GetText(FileType type, string key, params float[] valuesToReplace)
    {
        if (files[type].ContainsKey(key))
        {
            var str = files[type].GetString(key);
            for (var i = 0; i < valuesToReplace.Length; i++)
                str = str.Replace("{"+i.ToString()+"}", valuesToReplace[i].ToString());
            return str;
        }
        else
        {
            files[type].Add(key, key);
            return (key);
        }
    }

    public void SaveJSToFile(FileType type)
    {
        var path = "";
        if (type == FileType.Localization)
            path = "Assets/Resources/"+"texts_"+localizationFile+".json";
        if (type == FileType.Progression)
        {
            path = Application.persistentDataPath + "/"+profile+"/progression.json";
        }

        StreamWriter writer = new StreamWriter(path, false);
        writer.WriteLine(files[type].CreatePrettyString());
        writer.Close();
        
        LoadJSFromFile(type);
    }

    void OnApplicationQuit()
    {
        SaveJSToFile(FileType.Localization);
        SaveJSToFile(FileType.Progression);
    }

    public float GetFloatFromConfigForSkillLevel (string skill, string stat)
    {
        var level = (int)files[FileType.Progression].GetFloat("Skill_" + skill + "_Level", "Skills");
        var value = files[FileType.Config].GetFloat("Skill_" + skill + "_" + stat + "_" + level.ToString(), "Skills");
        return value;
    }
}
