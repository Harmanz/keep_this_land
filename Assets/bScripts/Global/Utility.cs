using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[System.Serializable]
public class Cooldown
{
    public float cd = 1f;
    public float timer = 1f;
    public float prob = 1f;
    bool active = true;


    public Cooldown(float cooldown)
    {
        cd = cooldown;
        timer = cooldown;
    }

    public Cooldown(float cooldown, bool activeOnStart)
    {
        cd = cooldown;
        timer = cooldown;
        active = activeOnStart;
    }

    public Cooldown(float cooldown, float probability)
    {
        cd = cooldown;
        timer = cooldown;
        prob = probability;
    }

    public Cooldown(float cooldown, float probability, bool activeOnStart)
    {
        cd = cooldown;
        timer = cooldown;
        prob = probability;
        active = activeOnStart;
    }

    public void SetRandomCooldown()
    {
        timer = Random.Range(0, cd);
    }

    public void ProcessWithoutCheck()
    {
        if(timer > 0)
            timer -= Time.deltaTime;
    }
    public void SetTimeToMax()
    {
        timer = cd;
    }
    public void SetTime(float time)
    {
        timer = time;
    }
    public bool Check(bool repeatable)
    {   
        if (!active)
            return false;
        if (cd == 0)
            return false;

        if (timer<0)
        {
            if (repeatable)
                timer=cd;
            if(prob == 1f)
                return(true);
            else
                return(Random.Range(0f,1f) <= prob);
        }
        else
        {
            timer-=Time.deltaTime;
            return(false);
        }
    }
    public bool Check()
    {   
        if (!active)
            return false;
        if (cd == 0)
            return false;
        if (timer < 0)
        {
            timer=cd;
            if(prob == 1f)
                return(true);
            else
                return(Random.Range(0f,1f) <= prob);
        }
        else
        {
            timer-=Time.deltaTime;
            return(false);
        }
    }
    public bool Check(float speed_mod)
    {   
        if (!active)
            return false;
        if (cd == 0)
            return false;
        if (timer < 0)
        {
            timer=cd;
            if(prob == 1f)
                return(true);
            else
                return(Random.Range(0f,1f) <= prob);
        }
        else
        {
            timer-=Time.deltaTime * speed_mod;
            return(false);
        }
    }
    public void SetActive(bool isActive)
    {
        active = isActive;
    }
    public bool IsReady()
    {
        return (timer <= 0);
    }
    public bool IsActive()
    {
        return (active);
    }
    public float GetTime()
    {
        return timer;
    }
}

[System.Serializable]
public class RandomFloat
{
    public float min = 0;
    public float max = 0;
    public bool randomSign = true;

    public float GetRandomValue()
    {
        if (randomSign)
            return (Random.Range(min, max+1) * Utility.RandomSign());
        else
            return Random.Range(min, max+1);
    }
}
[System.Serializable]
public class RandomInt
{
    public int min = 0;
    public int max = 0;
    public bool randomSign = true;

    public int GetRandomValue()
    {
        if (randomSign)
            return (Random.Range(min, max+1) * Utility.RandomSign());
        else
            return Random.Range(min, max+1);
    }

}
[System.Serializable]
public class RandomVector3
{
    public Vector3 min = Vector3.zero;
    public Vector3 max = Vector3.zero;
    public bool randomSign = true;

    public Vector3 GetRandomValue()
    {
        return Utility.GetRandomVector(min, max, randomSign);
    }
}

public static class Utility
{

    public static Quaternion GetAnglesModified(Quaternion original_angles, float x_mod, float y_mod, float z_mod)
    {
        var eulers = original_angles.eulerAngles;
        eulers.x += x_mod;
        eulers.y += y_mod;
        eulers.z += z_mod;
        return(Quaternion.Euler(eulers));

    }
    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        System.Type type = comp.GetType();
        if (type != other.GetType()) 
            return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos) 
        {
            if (pinfo.CanWrite) 
            {
                try 
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos) 
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }
    public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
    {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }
    public static Transform FindCharBodyPart(Transform character, string path)
    {
        return character.GetChild(0).Find(path);
    }
    public static bool Chance(float chance)
    {
        return (chance >= Random.Range(0f, 1f));
    }
    public static float RoundToDecimals(float value)
    {
        return(Mathf.Round(value*10)/10);
    }
    public static float ChangeValueWithElasticSpeed (float old_value, float new_value, float speed, float min, float max)
    {
        if (float.IsNaN(new_value))
            return(old_value);

        float sign = Mathf.Sign(new_value - old_value);
        float diff = Mathf.Abs(new_value - old_value);
        speed = Mathf.Clamp(diff / speed, min, max);
        speed = Mathf.Min(speed, diff);
       // Debug.Log("old "+old_value.ToString()+"  mew "+new_value.ToString()+"  speed "+speed.ToString());

        return(old_value+(speed * sign));
    }
    public static float ChangeValueWithSpeed (float old_value, float new_value, float speed)
    {
        float sign = Mathf.Sign(new_value - old_value);
        float diff = Mathf.Abs(new_value-old_value);
        return(old_value+(Mathf.Min(speed,diff)*sign));
    }
    public static float CutIfNotInRangeAbs(float value, float cutToMin, float min, float max, float cutToMax)
    {
        float sign = Mathf.Sign(value);
        value = Mathf.Abs(value);

        if (value < min)
            value = cutToMin;
        if (value > max)
            value = cutToMax;

        return(value*sign);
    }
    public static Color ChangeColorAlpha(Color color, float amount)
    {
        var col = color;
        col.a = Mathf.Clamp(col.a + amount, 0, 1);
        return col;
    }
    public static Color SetColorAlpha(Color color, float amount)
    {
        var col = color;
        col.a = amount;
        return col;
    }
    public static float Choose(params float[] arr)
    {
        var random_element = arr[Random.Range(0,arr.Length)];
        return random_element;
    }
    public static float CutIfNotInRange(float value, float cutToMin, float min, float max, float cutToMax)
    {
        if (value < min)
            value = cutToMin;
        if (value > max)
            value = cutToMax;
        return(value);
    }
    public static float VectorDirection(Vector3 from, Vector3 to)
    {
        Vector3 targetDir = to - from;
        var angle = Vector3.Angle(targetDir, Vector3.forward);
        if (from.x < to.x)
            return angle;
        else
            return angle*-1;
    }
    public static float VectorDirection(Transform from, Transform to)
    {
        Vector3 targetDir = to.position - from.position;
        var angle = Vector3.Angle(targetDir, from.forward);
        return angle;
    }
    public static float AngleDifference(float angle1, float angle2) 
    {
        float ad = Mathf.Max(angle1,angle2)-Mathf.Min(angle1,angle2);
        return(ad-180); // works wrong will fix later
    }
    public static float AngleDifferenceAbs(float angle1, float angle2) 
    {
        float ad = Mathf.Max(angle1,angle2)-Mathf.Min(angle1,angle2);
        return(180-Mathf.Abs(ad-180));
    }
    public static Vector3 PointInDirection(Vector3 start_point, float dir, float dist)
    {
        Quaternion angle = Quaternion.Euler(0, dir, 0);
        Vector3 tdirection = angle * Vector3.forward;
        Vector3 tpoint = tdirection * dist;
        return(start_point + tpoint);
    }
    public static Vector3 PointInDirection(Vector3 start_point, float dir, Quaternion relative_rot, float dist)
    {
        Quaternion angle = Quaternion.Euler(0, dir, 0);
        Vector3 tdirection = (angle * relative_rot) * Vector3.forward;
        Vector3 tpoint = tdirection * dist;
        return(start_point + tpoint);
    }
    public static float GetRandomValue(float value, float randomAmount)
    {
        if (randomAmount == 0)
            return value;
            
        var rand = value * randomAmount;
        return Random.Range(value - rand, value + rand);
    }
    public static float GetRandomValue(float value, float randomAmount, bool round)
    {
        if (randomAmount == 0)
            return value;
            
        var rand = value * randomAmount;
        if (!round)
            return Random.Range(value - rand, value + rand);
        else
            return Mathf.Round(Random.Range(value - rand, value + rand));
    }
    public static Vector3 GetRandomVector(Vector3 vector)
    {
        var randVector = new Vector3(Random.Range(-vector.x, vector.x),Random.Range(-vector.y, vector.y),Random.Range(-vector.z, vector.z));
        return randVector;
    }
    public static Vector3 GetRandomVector(Vector3 min, Vector3 max)
    {
        var randVector = new Vector3(Random.Range(min.x, max.x)*RandomSign(),Random.Range(min.y, max.y)*RandomSign(),Random.Range(min.z, max.z)*RandomSign());
        return randVector;
    }
    public static Vector3 GetRandomVector(Vector3 min, Vector3 max, bool random_sign)
    {
        if (random_sign)
        {
            var randVector = new Vector3(Random.Range(min.x, max.x)*RandomSign(),Random.Range(min.y, max.y)*RandomSign(),Random.Range(min.z, max.z)*RandomSign());
            return randVector;
        }
        else
        {
            var randVector = new Vector3(Random.Range(min.x, max.x),Random.Range(min.y, max.y),Random.Range(min.z, max.z));
            return randVector;
        }
    }
    public static int RandomSign()
    {
        return Random.value < .5? 1 : -1;
    }
    public static string RandomListString(List<string> list)
    {
        var random = Random.Range(0,list.Count);
        return (list[random]);
    }
    public static string NumbersWithSum(int count, int required_sum, int max)
    {
        var arr = new int[count];
        var sum = 0;
        var str = "";
        while (sum != required_sum)
        {
            sum = 0;
            str = "";
            for(var i =0; i<count; i++)
            {
                arr[i] = Random.Range(1,5);
                sum += arr[i];
                str+=" "+arr[i].ToString();
            }
        }
        return str;
    }
    public static void PlaySoundOnNewObject(string objName, AudioClip sound)
    {
        var soundObj = new GameObject(objName, typeof(AudioSource));
        var cAudio = soundObj.GetComponent<AudioSource>();
        cAudio.clip = sound;
        cAudio.Play();
        GameObject.Destroy(soundObj, sound.length);
    }
}