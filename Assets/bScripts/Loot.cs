﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Loot : MonoBehaviour
{
    [SerializeField] Image icon = null;
    [SerializeField] Image popUpRoot = null;
    [SerializeField] Text titleText = null;
    [SerializeField] Text descriptionText = null;
    [SerializeField] Text levelText = null;
    [SerializeField] float popUpShowDistance = 1f;
    [SerializeField] GameObject pickUpParticle = null;
    [SerializeField] Item item = null;
    [SerializeField] int level = 1;
    Transform player;
    PlayerStats cPlayerStats;
    FilesHandler filesHandler;
    //string[] romanNumbers = {"-","I","II","III","IV","V","VI","VII","VIII","IX","X"};
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        filesHandler = GameObject.FindGameObjectWithTag("FilesHandler").GetComponent<FilesHandler>();
        cPlayerStats = player.GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        popUpRoot.transform.eulerAngles = Camera.main.transform.eulerAngles;
        if (Vector3.Distance(transform.position, player.position) > popUpShowDistance)
            popUpRoot.gameObject.SetActive(false);
        else
        {
            if (icon.sprite == null)
                LoadDataFromItem();
            popUpRoot.gameObject.SetActive(true);
            if (Input.GetKey(KeyCode.F))
                PickUpLoot();
        }
    }

    private void LoadDataFromItem()
    {
        icon.sprite = item.iconSprite;
        var level = cPlayerStats.ItemGetLevel(item);
        titleText.text = filesHandler.GetText(FileType.Localization, item.name+"_Title");
        levelText.text = level.ToString();
        descriptionText.text = filesHandler.GetText(FileType.Localization, item.name+"_Description", item.GetValueForLevel(level) * 100);
        popUpRoot.rectTransform.sizeDelta = new Vector2(popUpRoot.rectTransform.rect.width, descriptionText.preferredHeight + 75);
    }

    private void PickUpLoot()
    {
        var particle = Instantiate(pickUpParticle, player.position, pickUpParticle.transform.rotation);
        Magic.DestroyParticleAfterPlay(particle);
        cPlayerStats.ItemAddWithAttachments(item, level);
        GameObject.Destroy(gameObject);
    }
}
