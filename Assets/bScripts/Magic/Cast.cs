﻿using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public enum CastIndicatorType
{
    Standart, Line, Circle
}
public enum CastCreateAt
{
    Caster, Target
}
[System.Serializable]
public class CastParams
{
    public Spell config;
    public float chargePlank = 1f;
    public int number = 1;
    public int angle = 0;
    public Vector3 addOffset = Vector3.zero;
}
[System.Serializable]
public class CastIndicator
{
    public CastIndicatorType type;
    public float scale = 1f;
}

[CreateAssetMenu(fileName = "Cast", menuName = "Magic/Make new cast", order = 1)]
public class Cast : ScriptableObject 
{
    [SerializeField] Sprite castIcon = null;
    [SerializeField] public DamageElement element;
    [SerializeField] AudioClip castSound = null;
    [SerializeField] float energyCost = 0; 
    [SerializeField] public CastCreateAt createAt = CastCreateAt.Caster;
    [SerializeField] Vector3 offset = new Vector3(0f, 1f, 0.5f);
    [SerializeField] bool canBeInterrupted = false;
    [SerializeField] float chargeTime = 1f;
    [SerializeField] public bool castAnim = true;
    [ConditionalField("castAnim", false, true)]  public int castAnimNum = 0;
    [SerializeField] bool canMoveWhileCharging = true;
    [SerializeField] float cooldownTime = 0f;
    [SerializeField] float spellLifeTimeModifier = 1;
    [SerializeField] CastIndicator indicator = null;
    [SerializeField] CastParams[] spellParameters = null;
    int level = 0; 
    int chosenSpellNum = -1;

    public void SetCastBasicBalues (float chargeTime, float spellLifeTimeModifier, float cooldownTime)
    {
        this.chargeTime = chargeTime;
        this.spellLifeTimeModifier = spellLifeTimeModifier;
        this.cooldownTime = cooldownTime;
    }

    public List<Magic> CastSpell(eTeam team, float time_charged, Transform caster, Transform target, float power)
    {
        var magics = new List<Magic>();
        var magic_prefab = (GameObject) Resources.Load("MagicPrefab");
        chosenSpellNum = GetChosenSpellNum(time_charged);
        if(chosenSpellNum == -1)
            return magics;
        var spellToCast = spellParameters[chosenSpellNum].config;
        var spellNum = spellParameters[chosenSpellNum].number;
        var halfNum = spellNum / 2;
        var castAngle = spellParameters[chosenSpellNum].angle;
        var anglePerSpell = castAngle/(float)spellNum;
        var startAngle = -(halfNum * anglePerSpell);

        var charPos = caster.position;
        if(createAt == CastCreateAt.Target)
            charPos = target.position;
        var castOffset = offset + spellParameters[chosenSpellNum].addOffset;
        var creationOffset = new Vector3(0,castOffset.y,0) + caster.right*castOffset.x;
        var creationSpread = spellToCast.random.creationSpread.GetRandomValue();
        creationSpread = caster.InverseTransformVector(creationSpread);
        var createPos = charPos + creationOffset + creationSpread;

        
        for(var i=0; i<spellNum; i++)
        {
            var flightSpread = spellToCast.random.flightSpread.GetRandomValue();
            var flightAngle = startAngle + flightSpread  + (anglePerSpell * i);
            var startPos = Utility.PointInDirection(createPos, flightAngle, caster.rotation, castOffset.z);
            var lookAtPos = Utility.PointInDirection(createPos, flightAngle, caster.rotation, 1 + Mathf.Abs(castOffset.z) * 2);

            var magic = Instantiate(magic_prefab, startPos, Quaternion.identity);
            magic.transform.name = caster.name+"_magic_"+this.name;
            magic.GetComponent<Team>().SetTeam(team);
            var cMagic = magic.GetComponent<Magic>();
            cMagic.cast = this;
            cMagic.power = power;
            cMagic.SetLifetimeModifier(time_charged, chargeTime, spellLifeTimeModifier);
            cMagic.Initialize(caster, spellToCast, target, lookAtPos);
            magics.Add(cMagic);
        }
        return magics;
    }

    public int GetChosenSpellNum(float time_charged)
    {
        // IF CAST LEVEL WAS SET MORE THAN ZERO IT MEANS THIS IS UPGRADEABLE SKILL THAT SPELL NUM SHOULD BE CHOSEN DUE TO LEVEL INSTEAD OF CHARGE TIME
        if(level > 0)
            return (level-1);

        float charge_percent;
        if (chargeTime == 0)
            charge_percent = 1;
        else
            charge_percent = Math.Min(1, time_charged / chargeTime);

        for(int i = spellParameters.Length-1; i>=0; i--)
            if (spellParameters[i].chargePlank <= charge_percent)
                return(i);
                
        return(-1);
    }
    public void SetLevel(int lvl)
    {
        level = lvl;
    }
    public CastIndicator GetIndicator()
    {
        return(indicator);
    }
    public float GetChargeTime()
    {
        return(chargeTime);
    }

    public float GetCooldown()
    {
        return(cooldownTime);
    }

    public bool CanMoveWhileCharging()
    {
        return canMoveWhileCharging;
    }
    
    public AudioClip GetCastSound()
    {
        return castSound;
    }
    
    public bool CanBeInterrupted()
    {
        return(canBeInterrupted);
    }
    
    public Sprite GetCastIcon()
    {
        return castIcon;
    }
    public float GetEnergyCost()
    {
        return energyCost;
    }
    private void OnEnable() 
    {
        try
        {
            foreach(var spellParam in spellParameters)
                spellParam.number = Mathf.Max(1, spellParam.number);
        }
        catch{}
    }
    private void OnAwake() 
    {
        try
        {
            foreach(var spellParam in spellParameters)
                spellParam.number = Mathf.Max(1, spellParam.number);
        }
        catch{}
    }
    public void OnValidate() {
        try
        {
            foreach(var spellParam in spellParameters)
                spellParam.number = Mathf.Max(1, spellParam.number);
            if (chargeTime > 0)
                castAnim = true;
        }
        catch{}
     }

}




