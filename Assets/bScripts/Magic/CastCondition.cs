﻿using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class CastSpecificCondition
{
    public string name;
    public bool active = false;
    public float setToZeroTime = 0f;
    Cooldown setToZeroTimer = null;

    public CastSpecificCondition()
    {
        this.name = "";
        this.active = false;
        this.setToZeroTimer = null;
    }

    public CastSpecificCondition(string name, bool active, float setToZeroTime)
    {
        this.name = name;
        this.active = active;
        this.setToZeroTimer = new Cooldown(setToZeroTime);
    }

    public void TrySetToZero()
    {
        if (setToZeroTimer == null)
            setToZeroTimer = new Cooldown(setToZeroTime);
        if(setToZeroTimer.Check())
            active = false;
    }
}

public enum CastConditionType
{
    DistanceToTarget, MyHealth, ReadyToAttack, TimeSinceGotDamage, MySpeed
}
public enum ConditionComparisonType
{
    Above,Equal,Below
}

[System.Serializable]
public class TypicalCastCondition
{
    public CastConditionType condition = CastConditionType.DistanceToTarget;
    public ConditionComparisonType shouldBe = ConditionComparisonType.Below;
    public bool revert = false;
    public float value = 0;
}

[CreateAssetMenu(fileName = "CastCondition", menuName = "Magic/Make new cast condition", order = 4)]
public class CastCondition : ScriptableObject 
{
    [SerializeField] aiState state = aiState.Fight;
    [SerializeField] bool hasTarget = false;
    [SerializeField] CastSpecificCondition specificCondition = new CastSpecificCondition(); 
    [SerializeField] public TypicalCastCondition[] typicalConditions;

    public CastSpecificCondition GetSpecificCondition()
    {
        return specificCondition;
    }

    public bool IsConditionsTrue(AIController ai)
    {
        if (state != aiState.None && state != ai.GetState())
            return false;
        if (hasTarget && ai.GetTarget() == null)
            return false;
        if(specificCondition.name != "")
        {
            Dictionary<string,CastSpecificCondition> aiSpecificConditions = ai.SpecificConditionsGetDictionary();
            if (aiSpecificConditions.ContainsKey(specificCondition.name) && !aiSpecificConditions[specificCondition.name].active)
                return false;
        }

        foreach (var tCondition in typicalConditions)
        {
            if (!IsTypicalConditionTrue(tCondition, ai))
                return false;
        }
        
        ai.SpecificConditionsSetActive(specificCondition.name, false);
        return true;
    }

    private bool IsTypicalConditionTrue(TypicalCastCondition tCondition, AIController ai)
    {
        float value = 0;
        if (tCondition.condition == CastConditionType.DistanceToTarget)
        {
            var target = ai.GetTarget();
            if (target == null)
                return false;
            else
                value = Vector3.Distance(ai.transform.position, target.transform.position);
        }
        if (tCondition.condition == CastConditionType.MyHealth)
            value = ai.GetComponent<Health>().GetHealthPercent();
        if (tCondition.condition == CastConditionType.ReadyToAttack)
            value = ai.GetComponent<Combat>().ReadyToAttack()? 1 : 0;
        if (tCondition.condition == CastConditionType.TimeSinceGotDamage)
            value = ai.GetComponent<Health>().TimeSinceDamage();
        if (tCondition.condition == CastConditionType.MySpeed)
            value = ai.GetComponent<NavMeshAgent>().speed;

        //Debug.Log(tCondition.condition.ToString() +" " + value.ToString());
        if (tCondition.shouldBe == ConditionComparisonType.Below && tCondition.value > value)
            return true;
        else if (tCondition.shouldBe == ConditionComparisonType.Above && tCondition.value < value)
            return true;
        else if (tCondition.shouldBe == ConditionComparisonType.Equal && tCondition.value == value)
            return true && !tCondition.revert;
        else
            return false || tCondition.revert;
    }
}




