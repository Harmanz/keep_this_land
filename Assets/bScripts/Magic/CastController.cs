﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[System.Serializable]
public class SkillBinding
{
    public eBinding binding;
    public Cast cast;
}

public class CastController : MonoBehaviour
{
    Dictionary<Cast, float> cooldowns = new Dictionary<Cast, float>();
    Cast currentCast = null;
    ChargeMarker cChargeMarker = null;
    Animator cAnimator = null;
    AIController cAIController = null;
    FamiliarController cFamiliar = null;
    CharModifiers cModifiers = null;
    bool allowCharge = true;
    float power = 1;
    float speedModifier = 1;
    void Start()
    {
        cAIController = GetComponent<AIController>();
        cFamiliar = GetComponent<FamiliarController>();
        cChargeMarker = GetComponentInChildren<ChargeMarker>();
        cChargeMarker.gameObject.SetActive(true);
        cAnimator = GetComponent<Animator>();
        cModifiers = GetComponent<CharModifiers>();
    }

    // Update is called once per frame
    void Update()
    {
        CooldownsProcess();
        CastCheck();
        cAnimator.SetBool("allowCharge", allowCharge);
    }

    public void SetPower(float power)
    {
        this.power = power;
    }
    
    public void SetSpeedModifier(float speedModifier)
    {
        this.speedModifier = speedModifier;
    }

    public bool CastStart(Cast cast)
    {
        if (CastIsCharging() || CooldownsIsCastOnCD(cast) || (cModifiers && cModifiers.IsModifierActive(ModifierType.DisableCast)))
            return false;
        var chargeTime = cast.GetChargeTime();
        cAnimator.SetInteger("castAnimNum", cast.castAnimNum);
        allowCharge = false;
        currentCast = cast;
        if (chargeTime == 0)
        {
            if (cast.castAnim)
            {
                cAnimator.SetTrigger("cast");
                AILookAtTarget();
            }
            else
                CastCreate();
        }
        else
        {
            cAnimator.SetTrigger("charge");
            cChargeMarker.StartCharge(chargeTime, cast.GetIndicator());
            AILookAtTarget();
        }
        return true;
    }

    private void AILookAtTarget()
    {
        if (cAIController != null && cAIController.GetTarget() != null)
            transform.LookAt(cAIController.GetTarget());
    }

    private void CastCheck()
    {
        if (cChargeMarker.IsCharging() && cChargeMarker.IsCharged())
            CastFinish();
    }

    public bool CastStopCharging()
    {
        if (cChargeMarker.IsCharging() && currentCast.CanBeInterrupted())
        {
            CastFinish();
            return true;
        }
        else
            return false;
    }

    public void CastFinish()
    {
        cChargeMarker.StopCharge();
        //CooldownsAddCast(currentCast);
        cAnimator.SetTrigger("cast");
    }

    public bool CanMoveWhileCharging()
    {
        return currentCast.CanMoveWhileCharging();
    }
    public bool CastIsCharging()
    {
        if(!allowCharge)
            return(true);
        return (cChargeMarker.IsCharging());
    }
    void Cast()// ANIM EVENT >
    {
        cAnimator.ResetTrigger("attack");
        CastCreate();
    }

    private void CastCreate()
    {
        CooldownsAddCast(currentCast);
        allowCharge = true;
        var target = transform;

        if (cAIController && cAIController.GetTarget() != null)
            target = cAIController.GetTarget();
        if (cFamiliar && cFamiliar.GetTarget() != null)
            target = cFamiliar.GetTarget();

        var castSound = currentCast.GetCastSound();
        if (castSound)
        {
            var soundObjName = transform.name + "_" + currentCast.ToString() + "_SND";
            Utility.PlaySoundOnNewObject(soundObjName, castSound);
        }
        var magics = currentCast.CastSpell(GetComponent<Team>().GetTeam(), cChargeMarker.GetChargedTime(), transform, target, power);
        foreach (var magic in magics)
            magic.flight.speed *= speedModifier;
    }

    private void CooldownsProcess()
    {
        foreach(Cast key in cooldowns.Keys.ToList())
        {
            if(cooldowns[key] > 0)
                cooldowns[key] -= Time.deltaTime;
            else
                cooldowns.Remove(key);
        }
    }

    private void CooldownsAddCast(Cast cast)
    {
        if (cooldowns.ContainsKey(cast))
            cooldowns[cast] = cast.GetCooldown();
        else
            cooldowns.Add(cast, cast.GetCooldown());
        
    }

    private bool CooldownsIsCastOnCD(Cast cast)
    {
        if (cooldowns.ContainsKey(cast))
            return(true);
        else
            return(false);
    }

    public float GetCastCooldown(Cast cast)
    {
        if (cast == null)
            return Mathf.Infinity;
            
        if (cooldowns.ContainsKey(cast))
        {
            //Debug.Log(cooldowns[cast]);
            return(cooldowns[cast]);
        }
        else
            return(0);
    }
}
