﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Werewolf.StatusIndicators.Components;

public class ChargeMarker : MonoBehaviour
{

    [SerializeField] public AudioSource audioSource = null;
    [SerializeField] StatusIndicator standartBar = null;
    [SerializeField] RangeIndicator circleBar = null;
    [SerializeField] AngleMissile lineBar = null;
    [SerializeField] Material pStandartCIMat = null;
    [SerializeField] Material pCircleCIMat = null;
    [SerializeField] Material pLineCIMat = null;
    [SerializeField] Material eStandartCIMat = null;
    [SerializeField] Material eCircleCIMat = null;
    [SerializeField] Material eLineCIMat = null;

    Splat currentIndicator;

    bool charge = false;
    public float timer = -1;
    float timerPrevious = 0;
    float timeToCharge = 1;
    float castSpeed = 1;
    void Start()
    {
        SetTimeToCharge(1.5f);
        TurnOffMarker();
        SetIndicatorMats();
    }

    // Update is called once per frame
    void Update()
    {
        if(charge)
        {
            if(timer<timeToCharge)
            {
                timer += Time.deltaTime * castSpeed;
                currentIndicator.Progress = currentIndicator == lineBar ? 0.5f + (timer/timeToCharge)/2 : timer/timeToCharge;
            }
        }
        else
          TurnOffMarker();
    }

    private void TurnOffMarker()
    {
        if (timer != 0)
        {
            timerPrevious = timer;
            timer = 0;

            standartBar.gameObject.SetActive(false);
            lineBar.gameObject.SetActive(false);
            circleBar.gameObject.SetActive(false);
        }
    }

    public void StartCharge(float timeToCharge, CastIndicator indicator)
    {
        SetTimeToCharge(timeToCharge);
        charge = true;
        if (timeToCharge == 0)
            return;

        if(indicator.type == CastIndicatorType.Standart)
        {
            currentIndicator = standartBar;
            currentIndicator.GetComponent<Projector>().orthographicSize = indicator.scale;
        } 
        if(indicator.type == CastIndicatorType.Circle)
        {
            currentIndicator = circleBar;
            currentIndicator.Scale = indicator.scale;
        }
        if(indicator.type == CastIndicatorType.Line)
        {
            currentIndicator = lineBar;
            currentIndicator.Scale = indicator.scale;
        }
        
        
        currentIndicator.gameObject.SetActive(true);
    }
    public void SetIndicatorMats()
    {
        var cTeam = transform.parent.GetComponent<Team>();
        if (cTeam)
        {
            var team = cTeam.GetTeam();
            var standartMat = team == eTeam.Player ? pStandartCIMat : eStandartCIMat;
            var lineMat = team == eTeam.Player ? pLineCIMat : eLineCIMat;
            var circleMat = team == eTeam.Player ? pCircleCIMat : eCircleCIMat;
            standartBar.GetComponent<Projector>().material = standartMat;
            circleBar.GetComponent<Projector>().material = circleMat;
            lineBar.transform.GetChild(0).GetComponent<Projector>().material = lineMat;
        }
    }
    public void SetTimeToCharge(float timeToCharge)
    {
        this.timeToCharge = timeToCharge;
    }

    public float GetChargedTime()
    {
        if (charge)
            return(timer);
        else
            return(timerPrevious);
    }

    public void StopCharge()
    {
        charge=false;
    }

    public bool IsCharged()
    {
        return(timeToCharge<=timer);
    }

    public bool IsCharging()
    {
        return(charge);
    }

    public void SetCastSpeed (float castSpeed)
    {
        this.castSpeed = castSpeed;
    }
}
