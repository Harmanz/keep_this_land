﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magic : MonoBehaviour
{

    MagicCollider spellCollider = null;

    bool moving = false;
    public SpellFlight flight = new SpellFlight(0,0,0,0);
    Cooldown accelerationStart;
    Cooldown rotationStart = new Cooldown(0f);
    Cooldown rotationEnd;
    SpellRotation spellRotation;
    SpellAutoAim autoAim;
    Cooldown autoAimStart = new Cooldown(0f);
    Cooldown autoAimEnd;

    GameObject prefabToCreate = null;

    float lifeTime = 0;
    float lifeTimeMod = 1;
    Cooldown refreshTimer = null;

    Transform caster = null;
    Transform target = null;
    Team cTeam = null;
    public Cast cast = null;
    public float power = 1;
    public Spell spell = null;
    

    void Update()
    {
        ProcessProjectileFlight();
        ProcessRefresh();
        ProcessLifetime();
    }

    public void Initialize(Transform caster, Spell spell, Transform target, Vector3 targetPoint)
    {
        if (spell == null)
        {
            Debug.LogError("Spell config is null");
            Destroy(this);
            return;
        }

        cTeam = GetComponent<Team>();
        this.caster = caster;
        this.spell = spell;
        this.target = target;
        transform.LookAt(targetPoint);

        ProcessSpellType(target, spell);
        ProcessEvent(SpellEvent.Start, target);
    }

    private void ProcessSpellType(Transform target, Spell spell)
    {
        lifeTime = spell.lifeTime * lifeTimeMod;
        refreshTimer = new Cooldown(spell.refreshTime);

        if (spell.GetSpellType() == SpellType.Projectile)
        {
            // FLIGHT SPEED AND ACCELERATION
            flight = new SpellFlight(spell.flight);
            if (flight.accelerationDelay > 0)
                accelerationStart = new Cooldown(Utility.GetRandomValue(flight.accelerationDelay, spell.random.accelerationDelay));
            else
                flight.SetAccelerationActive(true);
            // FLIGHT ROTATION
            spellRotation = new SpellRotation(spell.rotation);
            if (spellRotation.delay > 0)
                rotationStart = new Cooldown(spell.rotation.delay);
            else
                spellRotation.SetRotationActive(true);
            rotationEnd = new Cooldown(spell.rotation.time);
            // FLIGHT AUTO AIM
            autoAim = new SpellAutoAim(spell.autoAim);
            if (spell.autoAim.delay > 0)
                autoAimStart = new Cooldown(Utility.GetRandomValue(spell.autoAim.delay, spell.random.autoAimDelay));
            else
                autoAim.SetAimActive(true); 
            autoAimEnd = new Cooldown(spell.autoAim.time);
            //CREATING COLLIDER
            spellCollider = gameObject.AddComponent(typeof(MagicCollider)) as MagicCollider;
            spellCollider.Initialize(this, spell);
            moving = true;
        }

        if (spell.GetSpellType() == SpellType.CreateObject)
        {
            DestroyObjectsWithSameID();
            prefabToCreate = Instantiate(spell.prefabToCreate, transform.position, transform.rotation);
            if (spell.whoIsParent == SpellCreateObjectParent.Magic)
                prefabToCreate.transform.parent = transform;
            else if (spell.whoIsParent == SpellCreateObjectParent.Object)
                transform.parent = prefabToCreate.transform;
            else if (spell.whoIsParent == SpellCreateObjectParent.Caster)
            {
                prefabToCreate.transform.parent = transform;
                transform.parent = caster;
            }
            this.target = prefabToCreate.transform;
        }

        if (spell.GetSpellType() == SpellType.TargetSpell)
        {
            if (target != null)
            {
                transform.position = target.position;
                transform.parent = target; 
            }
        }
    }

    private void DestroyObjectsWithSameID()
    {
        if (spell.mustBeUnique)
            foreach (Magic magic in FindObjectsOfType(typeof(Magic)))
                if (magic != this)
                    magic.CheckObjectUniqueness(caster, spell.objectID);
    }

    private void ProcessEvent(SpellEvent spellEvent, Transform target)
    {
        InstantiateParticlesForEvent(spell, spellEvent, target);
        PlaySoundsForEvent(spell, spellEvent);
        CallEffectsForEvent(spell, spellEvent, target);
    }

    private void CallEffectsForEvent(Spell spell, SpellEvent spellEvent, Transform target)
    {
        if (target == null)
                target = transform;

        foreach (var spellEffect in spell.effects)
            if (spellEffect.eventName == spellEvent)
                spellEffect.effect.ProcessEffect(transform, caster, target, spellEffect.values.amount, spellEffect.values.duration, spellEffect.values.random);
    }

    private void PlaySoundsForEvent(Spell spell, SpellEvent spellEvent)
    {
        foreach (var spellSound in spell.sounds)
            if (spellSound.eventName == spellEvent)
            {
                var soundObjName = spell.ToString()+spellSound.eventName.ToString()+" sound";
                Utility.PlaySoundOnNewObject(soundObjName, spellSound.sound);
            }
    }

    private void InstantiateParticlesForEvent(Spell spell, SpellEvent spellEvent, Transform target)
    {
        if (target == null)
            target = transform;

        foreach (var spellParticle in spell.particles)
        {
            if (spellParticle.eventName == spellEvent)
            {
                GameObject newParticle = null;

                if (spellParticle.attach)
                {
                    if (spellParticle.destination == SpellParticleDestination.Magic)
                        newParticle = Instantiate(spellParticle.prefab, transform);
                    if (spellParticle.destination == SpellParticleDestination.Caster)
                        newParticle = Instantiate(spellParticle.prefab, caster);
                    if (spellParticle.destination == SpellParticleDestination.Target)
                        newParticle = Instantiate(spellParticle.prefab, target);
                }
                else
                {
                    if (spellParticle.destination == SpellParticleDestination.Magic)
                        newParticle = Instantiate(spellParticle.prefab, transform.position, transform.rotation);
                    if (spellParticle.destination == SpellParticleDestination.Caster)
                        newParticle = Instantiate(spellParticle.prefab, caster.position, caster.rotation);
                    if (spellParticle.destination == SpellParticleDestination.Target)
                        newParticle = Instantiate(spellParticle.prefab, target.position, target.rotation);
                }

                //if (spellParticle.scale == Vector3.zero)
                //    spellParticle.scale = Vector3.one;
                newParticle.transform.localScale = spellParticle.scale;

                if (newParticle.transform.parent == null)
                    newParticle.transform.position += spellParticle.shift;
                else
                    newParticle.transform.localPosition += spellParticle.shift;
                
                newParticle.transform.localRotation = Quaternion.Euler(spellParticle.rotation + newParticle.transform.localRotation.eulerAngles);
                if (newParticle != null)
                {
                    if (spellParticle.destroy == SpellParticleDestroy.AfterPlay)
                        DestroyParticleAfterPlay(newParticle);
                    else if (spellParticle.destroy == SpellParticleDestroy.BySpellLifetime)
                        Destroy(newParticle, lifeTime == (-1) ? Mathf.Infinity : lifeTime);
                }
            }
        }
    }

    private void ProcessProjectileFlight()
    {
        if (!moving)
            return;

        ProcessAutoAim();
        ProcessRotation();
        Move();
        ProcessAcceleration();

    }

    private void Move()
    {
        if (flight.speed != 0)
            transform.Translate(Vector3.forward * flight.speed * Time.deltaTime);
    }

    private void ProcessRotation()
    {
        if (spellRotation.speed != Vector3.zero)
        {
            if (spellRotation.RotationActive())
            {
                var euAngles = transform.rotation.eulerAngles;
                euAngles += spellRotation.speed * Time.deltaTime;
                transform.rotation = Quaternion.Euler(euAngles);
                spellRotation.speed += spellRotation.acceleration * Time.deltaTime;
                if (rotationEnd.Check(false))
                    spellRotation.SetRotationActive(false);
            }
            else
            {
                if (rotationStart.Check(false))
                {
                    rotationStart.SetActive(false);
                    spellRotation.SetRotationActive(true);
                }
            }
        }
    }

    private void ProcessAcceleration()
    {
        if (flight.AccelerationActive())
        {
            if (flight.speed != flight.maxSpeed)
                flight.speed = Utility.ChangeValueWithSpeed(flight.speed, flight.maxSpeed, flight.acceleration * Time.deltaTime);
        }
        else if (accelerationStart.Check())
            flight.SetAccelerationActive(true);
    }

    private void ProcessAutoAim()
    {
        if (autoAim.speed > 0 && target)
        {
            if (autoAim.AimActive())
            {
                var posToFollow = target.position;
                posToFollow.y = transform.position.y;
                var newDir = Vector3.RotateTowards(transform.forward, posToFollow - transform.position, autoAim.speed * Time.deltaTime, 0f);
                transform.rotation = Quaternion.LookRotation(newDir);
                if (autoAimEnd.Check(false))
                    autoAim.SetAimActive(false);
            }
            else
            {
                if (autoAimStart.Check(false))
                {
                    autoAimStart.SetActive(false);
                    autoAim.SetAimActive(true);
                }
            }
        }
    }

    private void ProcessRefresh()
    {
        if (refreshTimer.Check())
        {
            if (spellCollider != null)
                spellCollider.CollisionRefresh();
            ProcessEvent(SpellEvent.Refresh, target);
        }
    }

    public void ProcessCollision(Transform target, eTeam otherTeam)
    {
        ProcessEvent(SpellEvent.Collision, target);
        if (target == this.target)
            ProcessEvent(SpellEvent.TargetCollision, target);
        if (spell.onCollisionEvent == SpellOnCollision.None && otherTeam == eTeam.Environment)
            StopSpell();
        if (spell.onCollisionEvent == SpellOnCollision.Die)
            SelfDestroy();
        if (spell.onCollisionEvent == SpellOnCollision.Stop)
            StopSpell();
        if (spell.onCollisionEvent == SpellOnCollision.Bounce)
        {
            Bounce(0,10,0);
        }
    }

    public void Bounce(float rx, float ry, float rz)
    {
        var x_rand = UnityEngine.Random.Range(-rx, rx);
        var y_rand = 180 + UnityEngine.Random.Range(-ry, ry);
        var z_rand = UnityEngine.Random.Range(-rz, rz);
        transform.rotation = Utility.GetAnglesModified(transform.rotation, x_rand, y_rand, z_rand);
    }

    public static void DestroyParticleAfterPlay(GameObject particleInstance)
    {
        ParticleSystem partSys = particleInstance.GetComponent<ParticleSystem>();
        float destroyDelay = partSys.main.duration + partSys.main.startLifetime.constantMax;
        Destroy(particleInstance, destroyDelay);
    }

    private void ProcessLifetime()
    {
        if(lifeTime == -1)
            return;
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            ProcessEvent(SpellEvent.LifetimeEnd, target);
            SelfDestroy();
        }
    }

    public void ProcessExternalEvent()
    {
        ProcessEvent(SpellEvent.External, target);
    }

    public void CheckObjectUniqueness(Transform caster, string ID)
    {
        if (spell.objectID == ID && caster == this.caster)
            SelfDestroy();
    }

    public void SelfDestroy()
    {
        if (target == null)
            target = transform;

        ProcessEvent(SpellEvent.Death, target);
        StopSpell();
        Destroy(gameObject, 0.1f); // Added 0.1 delay because end particles could'nt appear
        this.enabled = false;
    }

    private void StopSpell()
    {
        flight.speed = 0;
        flight.acceleration = 0;
    }

    public void LookAtTargetPosition()
    {
        if (target!=null)
            transform.LookAt(target.position);
    }

    public void RenewLifetime()
    {
        lifeTime = spell.lifeTime;
        CallEffectsForEvent(spell, SpellEvent.Start, target);
    }

    public void SetLifetimeToMin()
    {
        lifeTime = 0;
    }

    public void SetLifetimeModifier(float chargeTime, float maxChargeTime, float modifier)
    {
        if (modifier > 0)
        {
            var chargeAmount = Mathf.Min(1, Mathf.Abs(chargeTime/maxChargeTime));
            lifeTimeMod =  chargeAmount * modifier;
        }
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
       // Debug.Log(target.name);
    }
    public Transform GetCaster()
    {
        return caster;
    }
}
