﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicCollider : MonoBehaviour
{
    SpellCollisionType collisionType=SpellCollisionType.Enemies;
    eTeam team = eTeam.EnemyGeneric;
    Magic myMagic = null;
    Spell spell = null;
    bool active = true;
    new Collider collider = null;
    List<Collider> collidersInAOE = new List<Collider>();
    List<Collider> collidedWith = new List<Collider>();

    public void Initialize(Magic magic, Spell spell)
    {
        if (spell.colliderType == ColliderType.None)
            return;

        myMagic = magic;
        this.spell = spell;
        GetTeamFromMagic();
        this.collisionType = spell.collisionType;

        if (spell.colliderType == ColliderType.Box)
        {
            this.collider = this.gameObject.AddComponent(typeof(BoxCollider)) as BoxCollider;
            ((BoxCollider)this.collider).size = spell.colliderSize;
            ((BoxCollider)this.collider).center = spell.colliderCenter;
            ((BoxCollider)this.collider).isTrigger = true;
        }
        if (spell.colliderType == ColliderType.Sphere)
        {
            this.collider = this.gameObject.AddComponent(typeof(SphereCollider)) as SphereCollider;
            ((SphereCollider)this.collider).radius = spell.colliderRadius;
            ((SphereCollider)this.collider).center = spell.colliderCenter;
            ((SphereCollider)this.collider).isTrigger = true;
        }
        if (spell.colliderType == ColliderType.Capsule)
        {
            this.collider = this.gameObject.AddComponent(typeof(CapsuleCollider)) as CapsuleCollider;
            ((CapsuleCollider)this.collider).radius = spell.colliderRadius;
            ((CapsuleCollider)this.collider).height = spell.colliderHeight;
            ((CapsuleCollider)this.collider).center = spell.colliderCenter;
            ((CapsuleCollider)this.collider).direction = (int)spell.colliderDirection;
            ((CapsuleCollider)this.collider).isTrigger = true;
        }
        if (spell.colliderType == ColliderType.Mesh)
        {
            this.collider = this.gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
            ((MeshCollider)this.collider).sharedMesh = spell.colliderMesh;
            ((MeshCollider)this.collider).convex = true;
            ((MeshCollider)this.collider).isTrigger = true;
        }

    }

    public void GetTeamFromMagic()
    {
        team = myMagic.GetComponent<Team>().GetTeam();
    }

    private void OnTriggerEnter(Collider other) 
    {
        //Debug.Log(other.name);
        InteractWithTrigger(other);
    }
    private void OnTriggerStay(Collider other)
    {
        InteractWithTrigger(other);
    }

    private void InteractWithTrigger(Collider other)
    {
        var enemy_catcher = other.GetComponent<ProjectileCatcher>();
        if(enemy_catcher)
            return;
        var enemyMagic = other.GetComponent<Magic>();
        if(enemyMagic)
            return;
        if (!active)
                return;

        if (!collidedWith.Contains(other))
            collidedWith.Add(other);
        else 
            return;
        
        Team otherTeamComponent = other.GetComponent<Team>();
        eTeam otherTeam;

        if (otherTeamComponent != null)
            otherTeam = otherTeamComponent.GetTeam();
        else
            otherTeam = eTeam.Environment;

        bool collisionCase = false;

        if (collisionType == SpellCollisionType.All)
            collisionCase = true;
        //else if (otherTeam == eTeam.Environment && collisionType == SpellCollisionType.Environment)
        else if (otherTeam == eTeam.Environment)
            collisionCase = true;
        else if (otherTeam != eTeam.Environment && collisionType == SpellCollisionType.Characters)
            collisionCase = true;
        else if (otherTeam != eTeam.Environment && otherTeam != team && collisionType == SpellCollisionType.Enemies)
            collisionCase = true;
        else if (otherTeam == team && collisionType == SpellCollisionType.Allies)
            collisionCase = true;

        if (collisionCase)
        {
            myMagic.ProcessCollision(other.transform, otherTeam);
            if (spell.onCollisionEvent == SpellOnCollision.Die)
                active = false;
        }
    }
    public void CollisionRefresh()
    {
        collidedWith.Clear();
    }

}