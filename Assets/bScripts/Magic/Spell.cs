using UnityEngine;
using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;



[CreateAssetMenu(fileName = "Spell", menuName = "Magic/Make new spell", order = 2)]
[System.Serializable]
public class Spell : ScriptableObject 
{
    [SerializeField] SpellType type = SpellType.Projectile;
    public float lifeTime = 10;
    [SerializeField] public float refreshTime = 0f;
    [ConditionalField("type", false, SpellType.Projectile)] public SpellFlight flight;
    [ConditionalField("type", false, SpellType.Projectile)] public SpellRotation rotation;
    [ConditionalField("type", false, SpellType.Projectile)] public SpellAutoAim autoAim;
    [SerializeField] public SpellRandom random;
    [ConditionalField("type", false, SpellType.Projectile)] public SpellCollisionType collisionType = SpellCollisionType.Enemies;
    [ConditionalField("type", false, SpellType.Projectile)] public SpellOnCollision onCollisionEvent = SpellOnCollision.Die;
    [ConditionalField("type", false, SpellType.Projectile)] public ColliderType colliderType = ColliderType.None;
    [ConditionalField("colliderType", false, ColliderType.Mesh)] public Mesh colliderMesh = null;
    [ConditionalField("colliderType", false, ColliderType.Box)] public Vector3 colliderSize = new Vector3(1,1,1);
    [ConditionalField("colliderType", false, ColliderType.Box, ColliderType.Capsule, ColliderType.Sphere)] public Vector3 colliderCenter = new Vector3(0,0,0);
    [ConditionalField("colliderType", false, ColliderType.Capsule, ColliderType.Sphere)] public float colliderRadius = 0.5f;
    [ConditionalField("colliderType", false, ColliderType.Capsule)] public float colliderHeight = 1f;
    [ConditionalField("colliderType", false, ColliderType.Capsule)] public ColliderDirection colliderDirection = ColliderDirection.Y;
    [ConditionalField("type", false, SpellType.CreateObject)] public SpellCreateObjectParent whoIsParent = SpellCreateObjectParent.Magic;
    [ConditionalField("type", false, SpellType.CreateObject)] public GameObject prefabToCreate = null;
    [ConditionalField("type", false, SpellType.CreateObject)] public bool mustBeUnique;
    [ConditionalField("type", false, SpellType.CreateObject)] public string objectID = System.Guid.NewGuid().ToString();
    
    [SerializeField] public SpellParticle[] particles;
    [SerializeField] public SpellEffectEntry[] effects;
    [SerializeField] public SpellSound[] sounds;
      

    public SpellType GetSpellType()
    {
        
        return (type);
    }
    private void OnValidate() 
    {
        try
        {
            foreach(var particle in particles)
            {
                if (particle.scale == Vector3.zero)
                    particle.scale = Vector3.one;
            }
        }
        catch{}
    }
}

public enum SpellCreateObjectParent { Magic, Object, None, Caster }

public enum SpellType { Projectile,TargetSpell,CreateObject}

public enum SpellCollisionType { All,Characters,Environment,Allies,Enemies }

public enum SpellOnCollision { None,Stop,Die,Bounce }

public enum ColliderType { None,Box,Sphere,Capsule,Mesh }

public enum ColliderDirection { X,Y,Z }

public enum SpellEvent { Start,Death,Collision,Refresh,LifetimeEnd,External,TargetCollision }

public enum SpellParticleDestination { Magic,Caster,Target }

public enum SpellParticleDestroy { AfterPlay, BySpellLifetime, Never }

[System.Serializable]
public class SpellParticle
{

    public GameObject prefab;
    public SpellEvent eventName;
    public SpellParticleDestination destination;
    public bool attach = true;
    public SpellParticleDestroy destroy;
    public Vector3 scale = Vector3.one;
    public Vector3 rotation = Vector3.zero;
    public Vector3 shift = Vector3.zero;

}

[System.Serializable]
public class SpellEffectEntry
{
    public SpellEffect effect;
    public SpellEvent eventName;
    public SpellEffectValues values;
}

[System.Serializable]
public class SpellEffectValues
{
    public float amount = 0;
    public float duration = 0;
    public float random = 0;
}

[System.Serializable]
public class SpellSound
{
    public AudioClip sound;
    public SpellEvent eventName;
}
[System.Serializable]
public class SpellAutoAim
{
    public float delay = 0;
    public float time = 0;
    public float speed = 0;
    bool active = false;

    public SpellAutoAim(SpellAutoAim aim)
    {
        this.delay = aim.delay;
        this.time = aim.time;
        this.speed = aim.speed;
    }

    public bool AimActive()
    {
        return active;
    }

    public void SetAimActive(bool active)
    {
        this.active = active;
    }
}

[System.Serializable]
public class SpellRotation
{
    public float delay = 0;
    public float time = 0;
    public Vector3 speed = Vector3.zero;
    public Vector3 acceleration = Vector3.zero;
    bool active = false;

    public SpellRotation(SpellRotation rotation)
    {
        this.delay = rotation.delay;
        this.time = rotation.time;
        this.speed = rotation.speed;
        this.acceleration = rotation.acceleration;
    }
    public bool RotationActive()
    {
        return active;
    }
    public void SetRotationActive(bool active)
    {
        this.active = active;
    }
}

[System.Serializable]
public class SpellFlight
{
    public float speed = 10;
    public float maxSpeed = 10;
    public float acceleration = 0;
    public float accelerationDelay = 0;
    bool accelerationActive = false;

    public SpellFlight (float speed, float maxSpeed, float acceleration, float accelerationDelay)
    {
        this.speed = speed;
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.accelerationDelay = accelerationDelay;
    }
    public SpellFlight (SpellFlight spellFlight)
    {
        this.speed = spellFlight.speed;
        this.maxSpeed = spellFlight.maxSpeed;
        this.acceleration = spellFlight.acceleration;
        this.accelerationDelay = spellFlight.accelerationDelay;
    }
    public bool AccelerationActive()
    {
        return accelerationActive;
    }
    public void SetAccelerationActive(bool active)
    {
        accelerationActive = active;
    }
}

[System.Serializable]
public class SpellRandom
{
    public float autoAimDelay = 0;
    public float accelerationDelay = 0;
    public RandomInt flightSpread;
    public RandomVector3 creationSpread;
}
