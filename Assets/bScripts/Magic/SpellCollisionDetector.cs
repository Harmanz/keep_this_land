﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCollisionDetector : MonoBehaviour
{
    [SerializeField] Spell spellToDetect = null;
    [SerializeField] bool destroyCollisionOnReact = false;
    [SerializeField] bool sendEventToParent = true;
    [SerializeField] bool sendEventToCollision = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other) 
    {
        var otherMagic = other.GetComponent<Magic>();
        if (otherMagic && otherMagic.spell == spellToDetect)
        {
            var parentMagic = transform.parent.GetComponent<Magic>();
            if (parentMagic && sendEventToParent)
                parentMagic.ProcessExternalEvent();
            if (sendEventToCollision)
                otherMagic.ProcessExternalEvent();
            if (destroyCollisionOnReact)
                otherMagic.SetLifetimeToMin();
        }
    }   
}
