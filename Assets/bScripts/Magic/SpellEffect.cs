﻿using UnityEngine;
using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "SpellEffect", menuName = "Magic/Make new effect", order = 3)]
[System.Serializable]
public class SpellEffect : ScriptableObject 
{
    [SerializeField] SpellEffectType type = SpellEffectType.Damage;
    [ConditionalField("type", true, SpellEffectType.Spell, SpellEffectType.Magic, SpellEffectType.ChooseTarget)] public SpellEffectTargets targets = SpellEffectTargets.Enemies;
    [ConditionalField("type", false, SpellEffectType.Modifier)] public ModifierType modifierType = ModifierType.Speed;
    [ConditionalField("type", false, SpellEffectType.Transform)] public TransformEffectType transformEffectType = TransformEffectType.MoveToMagic;
    [ConditionalField("type", false, SpellEffectType.Magic)] public SpellMagicEffectType magicEffectType = SpellMagicEffectType.SetLifetimeToMin;
    [ConditionalField("type", false, SpellEffectType.ChooseTarget)] public SpellSetTargetEffectType newTarget;
    [ConditionalField("magicEffectType", false, SpellMagicEffectType.CreatePrefabInside)] public GameObject prefabToCreate;
    [ConditionalField("type", false, SpellEffectType.Other)] public OtherEffectType otherEffectType = OtherEffectType.EnableVisualEntity;
    [ConditionalField("otherEffectType", false, OtherEffectType.SetAnimParam)] public AnimParamType animParameterType;
    [ConditionalField("otherEffectType", false, OtherEffectType.SetAnimParam)] public string animParameterName;
    [ConditionalField("animParameterType", false, AnimParamType.Bool)] public bool animParamValueBool;
    [ConditionalField("animParameterType", false, AnimParamType.Int)] public int animParamValueInt;
    [ConditionalField("animParameterType", false, AnimParamType.Int)] public bool randomFromZeroToValue;
    [ConditionalField("animParameterType", false, AnimParamType.Float)] public float animParamValueFloat;
    [ConditionalField("type", false, SpellEffectType.SetSpecificCondition)] public string condition = "";
    [ConditionalField("type", false, SpellEffectType.Spell)] public Cast cast = null;
    [ConditionalField("type", false, SpellEffectType.Spell)] public bool saveCaster = false;
    [ConditionalField("type", false, SpellEffectType.Spell)] public SpellEffectSameSpellsAction sameSpellsOnTarget = SpellEffectSameSpellsAction.None;
    [ConditionalField("type", false, SpellEffectType.Spell)] public bool dontCastIfSpellExists = false;
    [ConditionalField("type", false, SpellEffectType.Spell)] public SpellEffectSameSpellsAction sameSpellsOnOthers = SpellEffectSameSpellsAction.None;
    [ConditionalField("type", false, SpellEffectType.ExchangeSpellCharges)] public Cast castToCheck = null;
    [ConditionalField("type", false, SpellEffectType.ExchangeSpellCharges)] public Cast castForExchange = null;
    [ConditionalField("type", false, SpellEffectType.ExchangeSpellCharges)] public bool checkOnTarget = true;
    [ConditionalField("type", false, SpellEffectType.ExchangeSpellCharges)] public bool checkOnOthers = false;
    [ConditionalField("type", false, SpellEffectType.ExchangeSpellCharges)] public bool createOnChargeMagics = false;
    [ConditionalField("type", false, SpellEffectType.ExchangeSpellCharges)] public int chargesCount = 0;

    [SerializeField] string effectID = System.Guid.NewGuid().ToString();

    public void ProcessEffect(Transform magic, Transform caster, Transform target, float amount, float duration, float random)
    {

        if (!TeamsAreValid(magic, target))
            return;
        if(!caster)
            caster = magic;

        amount = Utility.GetRandomValue(amount, random);
        amount *= magic.GetComponent<Magic>().power;

        if (type == SpellEffectType.Modifier)
            ApplyEffectOnTime(magic, caster, target, amount, duration);
        else
            ApplySingleEffect(magic, caster, target, amount, duration);
    }

    private void ApplySingleEffect(Transform magic, Transform caster, Transform target, float amount, float duration)
    {
        Transform effectTarget = PickEffectTarget(magic, caster, target);
        
        if (type == SpellEffectType.Impulse)
            ApplyImpulse(magic, amount, effectTarget);

        if (type == SpellEffectType.Damage)
            ApplyDamageEffect(magic, amount,caster, effectTarget);

        if (type == SpellEffectType.Heal)
            ApplyHealingEffect(amount, effectTarget);

        if (type == SpellEffectType.Spell)
            CastSpellFromEffect(cast, caster, magic, effectTarget);

        if (type == SpellEffectType.ExchangeSpellCharges)
            ExchangeSpellCharges(castForExchange, caster, magic, effectTarget);

        if (type == SpellEffectType.Transform)
            ApplyTransformTypeEffect(magic, amount, effectTarget);

        if (type == SpellEffectType.Magic)
            ApplyMagicTypeEffect(magic, caster, effectTarget);
            
        if (type == SpellEffectType.ChooseTarget)
            ApplyTargetChoiceEffect(magic, caster);

        if (type == SpellEffectType.Other)
            ApplyOtherTypeEffect(effectTarget);

        if (type == SpellEffectType.SetSpecificCondition)
            effectTarget.GetComponent<AIController>().SpecificConditionsSetActive(condition, true);

    }

    private static void ApplyHealingEffect(float amount, Transform effectTarget)
    {
        Health targetHealth = effectTarget.GetComponent<Health>();
        if (targetHealth)
            targetHealth.GetHealing(amount);
    }

    private static void ApplyImpulse(Transform magic, float amount, Transform effectTarget)
    {
        var cKnockback = effectTarget.GetComponent<AgentKnockback>();
        if (cKnockback)
        {
            var dir = (effectTarget.position - new Vector3(magic.position.x, effectTarget.position.y, magic.position.z)).normalized;
            cKnockback.pos = cKnockback.pos + (dir * amount);
            cKnockback.GetComponent<MovementController>().CancelMovement();
        }
    }

    private static void ApplyDamageEffect(Transform magic, float amount,Transform caster, Transform effectTarget)
    {
        //Debug.Log(magic.name + " "+ amount.ToString()+ " "+effectTarget.name);
        Health targetHealth = effectTarget.GetComponent<Health>();
        Magic cMagic = magic.GetComponent<Magic>();
        if (targetHealth)
        {
            var dType = (cMagic.cast.GetEnergyCost() > 0) ? DamageType.EnergySpell : DamageType.Spell;
            targetHealth.GetDamage(amount, caster, false, dType, cMagic.cast.element);
        }
    }

    private Transform PickEffectTarget(Transform magic, Transform caster, Transform target)
    {
        Transform effectTarget = null;

        if (targets == SpellEffectTargets.Caster)
            effectTarget = caster;
        else if (targets == SpellEffectTargets.Magic)
            effectTarget = magic;
        else if (targets == SpellEffectTargets.Parent)
            effectTarget = magic.parent;
        else
            effectTarget = target;

        //Debug.Log(effectTarget);
        return effectTarget;
    }

    private void ApplyOtherTypeEffect(Transform effectTarget)
    {
        if (otherEffectType == OtherEffectType.EnableVisualEntity)
        {
            //Transform targetVisual = effectTarget.Find("visual"); // I SHOULD REWORK THIS AND SEARCH FOR ENTITY WITH COMPONENTS INSTEAD OF NAME
            //effectTarget.gameObject.GetComponentInChildren
            //Transform targetVisual = effectTarget.GetComponentInChildren<SkinnedMeshRenderer>().transform;
            var skinnedMeshRenderer = effectTarget.GetComponentInChildren<SkinnedMeshRenderer>();
            if (skinnedMeshRenderer)
                skinnedMeshRenderer.enabled = true;
           // targetVisual.gameObject.SetActive(true);
        }
        if (otherEffectType == OtherEffectType.DisableVisualEntity)
        {
            //Transform targetVisual = effectTarget.Find("visual");
            //Transform targetVisual = effectTarget.GetComponentInChildren<SkinnedMeshRenderer>().transform;
            //targetVisual.gameObject.SetActive(false);
            var skinnedMeshRenderer = effectTarget.GetComponentInChildren<SkinnedMeshRenderer>();
            if (skinnedMeshRenderer)
                skinnedMeshRenderer.enabled = false;
        }
        if (otherEffectType == OtherEffectType.MakeInvulnerable)
        {
            var cHealth = effectTarget.GetComponent<Health>();
            if (cHealth)
                cHealth.SetInvulnerability(true);
        }
        if (otherEffectType == OtherEffectType.MakeVulnerable)
        {
            var cHealth = effectTarget.GetComponent<Health>();
            if (cHealth)
                cHealth.SetInvulnerability(false);
        }
        if (otherEffectType == OtherEffectType.Kill)
        {
            var cHealth = effectTarget.GetComponent<Health>();
            if (cHealth)
                cHealth.Die();
        }
        if (otherEffectType == OtherEffectType.SetAnimParam)
        {
            var cAnimator = effectTarget.GetComponent<Animator>();
            if (cAnimator)
            {
                if (animParameterType == AnimParamType.Trigger)
                    cAnimator.SetTrigger(animParameterName);
                if (animParameterType == AnimParamType.Float)
                    cAnimator.SetFloat(animParameterName, animParamValueFloat);
                if (animParameterType == AnimParamType.Int)
                {
                    var value = randomFromZeroToValue ? UnityEngine.Random.Range(0,animParamValueInt) : animParamValueInt;
                    cAnimator.SetInteger(animParameterName, value);
                }
                if (animParameterType == AnimParamType.Bool)
                    cAnimator.SetBool(animParameterName, animParamValueBool);
            }
        }
    }

    private void ApplyMagicTypeEffect(Transform magic, Transform caster, Transform effectTarget)
    {
        if (magicEffectType == SpellMagicEffectType.CreatePrefabInside)
        {
            Instantiate(prefabToCreate, magic);
        }
        if (magicEffectType == SpellMagicEffectType.SetLifetimeToMin)
        {
            magic.GetComponent<Magic>().SetLifetimeToMin();
        }
        if (magicEffectType == SpellMagicEffectType.AttachToCaster)
        {
            magic.parent = caster;
        }
        if (magicEffectType == SpellMagicEffectType.GetPlayerRotation)
        {
            var player = GameObject.FindObjectOfType<PlayerController>().transform;
            var euAng = magic.rotation.eulerAngles;
            var casterEuAng = player.rotation.eulerAngles;
            euAng.y = casterEuAng.y;
            magic.rotation = Quaternion.Euler(euAng);
        }
        if (magicEffectType == SpellMagicEffectType.LookAtCaster)
        {
            magic.LookAt(caster.position + Vector3.up);
        }
        if (magicEffectType == SpellMagicEffectType.SetCasterAsTarget)
        {
            magic.GetComponent<Magic>().SetTarget(caster);
        }
        if (magicEffectType == SpellMagicEffectType.AttachToTarget)
        {
            magic.parent = effectTarget;
        }
    }

    private void ApplyTargetChoiceEffect(Transform magic, Transform caster)
    {
        if (newTarget == SpellSetTargetEffectType.Caster)
        {
            magic.GetComponent<Magic>().SetTarget(caster);
        }
        if (newTarget == SpellSetTargetEffectType.NearestEnemy)
        {
            var magicTeam = magic.GetComponent<Team>();
            var enemy = magicTeam.GetClosestTeamMember(magicTeam.GetOppositeTeam(), magic.position, 50);
            magic.GetComponent<Magic>().SetTarget(enemy);
        }
        if (newTarget == SpellSetTargetEffectType.RandomEnemy)
        {
            var magicTeam = magic.GetComponent<Team>();
            var enemy = magicTeam.GetRandomTeamMember(magicTeam.GetOppositeTeam());
            magic.GetComponent<Magic>().SetTarget(enemy);
        }
    }

    private void ApplyTransformTypeEffect(Transform magic, float amount, Transform effectTarget)
    {
        if (transformEffectType == TransformEffectType.MoveToMagic)
        {
            effectTarget.position = magic.position; // IT WORKS IF SET CASTER DIRECTLY BUUUUT BUT BUT BUT SHOULD BE USED ON TARGET THAT SET IN EFFECT SETTINGS
        }
        if (transformEffectType == TransformEffectType.AttachToMagic)
        {
            effectTarget.parent = magic;
        }
        if (transformEffectType == TransformEffectType.DetachFromParent)
        {
            effectTarget.parent = null;
        }
        if (transformEffectType == TransformEffectType.TurnByY)
        {
            var euAng = effectTarget.rotation.eulerAngles;
            euAng.y += amount;
            effectTarget.rotation = Quaternion.Euler(euAng);
        }
    }

    private void ApplyEffectOnTime(Transform magic, Transform caster, Transform target, float amount, float duration)
    {
        Transform effectTarget = null;
        if (targets == SpellEffectTargets.Caster)
            effectTarget = caster;
        else
            effectTarget = target;

        if (type == SpellEffectType.Modifier)
        {
            CharModifiers targetModifiers = effectTarget.GetComponent<CharModifiers>();
            if (targetModifiers != null)
                targetModifiers.SetValue(magic.GetComponent<Magic>().cast.name, modifierType, amount, duration);
        }
    }

    public void CastSpellFromEffect(Cast cast, Transform caster, Transform magic, Transform target)
    {
        var spellExists = false;
        foreach(var magicEntity in GameObject.FindGameObjectsWithTag("Magic"))
        {
            var cMagic = magicEntity.GetComponent<Magic>();
            if (cMagic.cast == cast)
            {
                if (magicEntity.transform.parent == target)
                {
                    spellExists = true;
                    if (sameSpellsOnTarget == SpellEffectSameSpellsAction.Destroy)
                        cMagic.SetLifetimeToMin();
                    else if (sameSpellsOnTarget == SpellEffectSameSpellsAction.Renew)
                        cMagic.RenewLifetime();
                }
                else
                {
                    if (sameSpellsOnOthers == SpellEffectSameSpellsAction.Destroy)
                        cMagic.SetLifetimeToMin();
                    else if (sameSpellsOnOthers == SpellEffectSameSpellsAction.Renew)
                        cMagic.RenewLifetime();
                }
            }
        }
        
        if (!spellExists || !dontCastIfSpellExists)
            cast.CastSpell(magic.GetComponent<Team>().GetTeam(), cast.GetChargeTime(), saveCaster? caster : magic, target, magic.GetComponent<Magic>().power);
    }

    public void ExchangeSpellCharges(Cast cast, Transform caster, Transform magic, Transform target)
    {
        var chargeEntities = new List<Magic>();
        var charges = 0;
        foreach(var magicEntity in GameObject.FindGameObjectsWithTag("Magic"))
        {
            var cMagic = magicEntity.GetComponent<Magic>();
            if (cMagic.cast == castToCheck && ((magicEntity.transform.parent == target && checkOnTarget) || (checkOnOthers && magicEntity.transform.parent != target)))
            {
                charges++;
                chargeEntities.Add(cMagic);
            }
        }
        if (charges >= chargesCount)
        {
            foreach (var charge in chargeEntities)
                //Destroy(charge.gameObject);
                charge.SetLifetimeToMin();
    
            if(createOnChargeMagics)
            {
                foreach (var charge in chargeEntities)
                    cast.CastSpell(caster.GetComponent<Team>().GetTeam(), cast.GetChargeTime(), charge.transform, target, magic.GetComponent<Magic>().power);
            }
            else
                cast.CastSpell(caster.GetComponent<Team>().GetTeam(), cast.GetChargeTime(), magic, target, magic.GetComponent<Magic>().power);
        }
    }

    public bool TeamsAreValid(Transform caster,Transform target)
    {
        if(type == SpellEffectType.Spell)
            return true;
        if(type == SpellEffectType.Magic)
            return true;
        if(type == SpellEffectType.ChooseTarget)
            return true;
        if (target.GetComponent<Team>() == null)
            return true;
            
        eTeam casterTeam = caster.GetComponent<Team>().GetTeam();
        eTeam targetTeam = target.GetComponent<Team>().GetTeam();

        if (casterTeam != targetTeam && targets == SpellEffectTargets.Allies)
            return false;
        if (casterTeam == targetTeam && targets == SpellEffectTargets.Enemies)
            return false;
 
        return true;
    }
}

enum SpellEffectType { Damage,Heal,Impulse,Spell,Modifier,Transform,Other,SetSpecificCondition,ExchangeSpellCharges, Magic, ChooseTarget}
public enum AnimParamType {Trigger, Bool, Float, Int}
public enum SpellEffectSameSpellsAction { None,Renew,Destroy }
public enum ModifierType { Speed,DisableMovement,DisableAttack,DisableCast,DisableAnim,DamageIncome,DamageOutcome, AngularSpeed }
public enum TransformEffectType { MoveToMagic, AttachToMagic, DetachFromParent, TurnByY }
public enum OtherEffectType { DisableVisualEntity,EnableVisualEntity, SetAnimParam, MakeInvulnerable, MakeVulnerable, Kill}
public enum SpellEffectTargets { Caster,Allies,Enemies,All, Magic, Parent }
public enum SpellMagicEffectType { CreatePrefabInside, SetLifetimeToMin, AttachToCaster, GetPlayerRotation, LookAtCaster, SetCasterAsTarget, AttachToTarget }
public enum SpellSetTargetEffectType { Caster, NearestEnemy, RandomEnemy }