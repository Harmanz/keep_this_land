﻿using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum eBinding
{
    None = -1,
    SkillCrowdControl,SkillDamage,SkillMovement,SkillDefense,SkillTest,ChargedFirst,ChargedSecond,ChargedThird,ChargedFourth
}
[System.Serializable]
public class Binding
{
    public eBinding bindingName = eBinding.None;
    public string[] buttons;
    KeyCode[] codes = null;

    public void SetCodes()
    {
        codes = new KeyCode[buttons.Length];
        for (int i=0; i<buttons.Length; i++)
        {
            KeyCode buttonKC = (KeyCode) Enum.Parse(typeof(KeyCode), buttons[i]);
            codes[i] = buttonKC;
        }
    }
    public KeyCode[] GetCodes()
    {
        return codes;
    }
}

[CreateAssetMenu(fileName = "Control bindings", menuName = "Make new control bindings", order = 1)]
public class ControlBindings : ScriptableObject 
{
    [SerializeField] Binding[] Bindings = null;

    private void OnEnable() 
    {
        SetCodes();
        for (var i = 0; i < Bindings.Length; i++)
            Bindings[i].bindingName = (eBinding)i;
    }
    public void SetCodes()
    {
        foreach (Binding binding in Bindings)
            binding.SetCodes();
    }

    public KeyCode BindingDown(eBinding bindingName)
    {

        foreach (KeyCode code in Bindings[(int)bindingName].GetCodes())
        {
            if (Input.GetKey(code))
                return code;
        }
        return 0;
    }

    public Binding[] GetBindings()
    {
        return Bindings;
    }

}




