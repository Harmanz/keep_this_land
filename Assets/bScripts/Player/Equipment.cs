﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    [SerializeField] EquipmentItem[] items = null;
    [SerializeField] Transform charVisual = null;
    string[] bodyPartPath = 
    {
        "Root/Hips/Spine_01/Spine_02/Spine_03/Neck", // Head
        "Root/Hips/Spine_01/Spine_02/Spine_03/Neck/Head/Eyebrows", // Eyebrow
        "Root/Hips/Spine_01/Spine_02/Spine_03/Neck/Head", // FacialHair
        "Root/Hips", // Torso
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_L", // ArmUpperLeft
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_R", // ArmUpperRight
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_L/Shoulder_L", // ArmLowerLeft
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_R/Shoulder_R", // ArmLowerRight
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_L/Shoulder_L/Elbow_L/Hand_L", // HandLeft
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_R/Shoulder_R/Elbow_R/Hand_R", // HandRight
        "Root/Hips", // Hip
        "Root/Hips/UpperLeg_L/LowerLeg_L", // LegLeft
        "Root/Hips/UpperLeg_R/LowerLeg_R", // LegRight
        "Root/Hips/Spine_01/Spine_02/Spine_03", // Hair
        "Root/Hips/Spine_01/Spine_02/Spine_03", // HeadCovering
        "Root/Hips/Spine_01/Spine_02/Spine_03", // Helmet
        "Root/Hips/Spine_01/Spine_02/Spine_03/Neck/Head", // ElfEar
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_L/Shoulder_Attachment_L", // ShoulderLeft
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_R/Shoulder_Attachment_R", // ShoulderRight
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_L/Shoulder_L/Elbow_L/Elbow_Attachment_L", // ElbowLeft
        "Root/Hips/Spine_01/Spine_02/Spine_03/Clavicle_R/Shoulder_R/Elbow_R/Elbow_Attachment_R", // ElbowRight
        "Root/Hips/Spine_01/Spine_02/Spine_03/Back_Attachment", // BackAtt
        "Root/Hips", // Hips
        "Root/Hips/UpperLeg_L/LowerLeg_L/Knee_Attachment_L", // KneeLeft
        "Root/Hips/UpperLeg_R/LowerLeg_R/Knee_Attachment_R" // KneeRight
        //
    };

    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in items)
        {
            item.Equip(charVisual, bodyPartPath);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
