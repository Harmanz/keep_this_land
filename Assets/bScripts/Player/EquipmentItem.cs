﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using MyBox;

public enum ItemBodyPart
{
    Head, 
    Eyebrow, 
    FacialHair, 
    Torso,
    ArmUpperLeft, 
    ArmUpperRight, 
    ArmLowerLeft, 
    ArmLowerRight, 
    HandLeft, 
    HandRight,
    Hip, 
    LegLeft, 
    LegRight,
    Hair, 
    HeadCovering, 
    Helmet, 
    ElfEar,
    ShoulderLeft, 
    ShoulderRight, 
    ElbowLeft, 
    ElbowRight, 
    BackAtt, 
    Hips, 
    KneeLeft, 
    KneeRight
}
[System.Serializable]
public class ItemVisualPart
{
    public GameObject prefab;
    public ItemBodyPart bodyPart;

}
[CreateAssetMenu(fileName = "Equipment", menuName = "Characters/Make new equipment item", order = 2)]
[System.Serializable]
public class EquipmentItem : ScriptableObject 
{
    public ItemVisualPart[] parts;
    [Header("Colors")]
    public Color primary = new Color(0.4313726f, 0.2313726f, 0.2705882f);
    public Color secondary = new Color(0.7019608f, 0.6235294f, 0.4666667f);
    public Color primaryMetal = new Color(0.6705883f, 0.6705883f, 0.6705883f);
    public Color secondaryMetal = new Color(0.6705883f, 0.6705883f, 0.6705883f);
    public Color leatherPrimary = new Color(0.3098039f, 0.254902f, 0.1764706f);
    public Color leatherSecondary =new Color(0.2431373f, 0.2039216f, 0.145098f);
    public Color skin = new Color(1f, 0.8000001f, 0.682353f);
    public Color hair = new Color(0.1764706f, 0.1686275f, 0.1686275f);
    public Color scar = new Color(0.9294118f, 0.6862745f, 0.5921569f);
    public Color bodyArt = new Color(0.0509804f, 0.6745098f, 0.9843138f);


    public void Equip(Transform charVisual, string[] partsPath)
    {
        foreach (var part in parts)
        {
            //Debug.Log(partsPath[(int)part.bodyPart]);
            Transform objectToAttach = charVisual.Find(partsPath[(int)part.bodyPart]);
            //Debug.Log(objectToAttach.name);
            var obj = Instantiate(part.prefab, objectToAttach);
            //var mesh = obj.GetComponent<MeshFilter>().mesh;
            //Destroy(obj.GetComponent<MeshFilter>());
            //Destroy(obj.GetComponent<MeshRenderer>());
            //this.collider = this.gameObject.AddComponent(typeof(BoxCollider)) as BoxCollider;
            //var meshRenderer = obj.gameObject.AddComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            //meshRenderer.updateWhenOffscreen = true;
           // meshRenderer.rootBone = objectToAttach;
           if(obj.GetComponent<SkinnedMeshRenderer>())
           obj.GetComponent<SkinnedMeshRenderer>().rootBone = objectToAttach;
        }
    }
}


