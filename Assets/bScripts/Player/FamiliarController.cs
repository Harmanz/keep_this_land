﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TigerForge;

public class FamiliarController : MonoBehaviour
{
    [SerializeField] float minDistance = 5;
    [SerializeField] float maxDistance = 10;
    [SerializeField] GameObject[] energyFeedbackParticles = null;
    [SerializeField] GameObject energyChangeParticle = null;
    [SerializeField] GameObject[] forms;
    [HeaderAttribute ("Weapon VFX")]
    [SerializeField] GameObject beamMain = null;
    [SerializeField] GameObject beamStart = null;
    [SerializeField] GameObject beamEnd = null;
    [HeaderAttribute ("Attack")]
    [SerializeField] bool attack;
    [SerializeField] float damagePerAttack = 30;
    [SerializeField] float attackCooldown = 3;
    float slowTime = 0;
    float slowPower = 0;
    float stunTime = 0;
    float damagePeriod = 0.3f;
    float attackTime = 1.2f;
    float damagePerPeriod = 0;
    int maxAttacksInAttackTime;
    int attacksLeft;
    Team cTeam = null;
    Cooldown attemptToGetTargetTimer = new Cooldown(0.5f);
    Cooldown attackTimer;
    Cooldown damageTimer;
    Transform target;
    GameObject player;
    PlayerController cPlayerControl;
    PlayerStats cPlayerStats;
    NavMeshAgent cAgent;
    float acceleration = 1.5f;
    float breakingSpeed = 6;
    float maxSpeed = 5.6f;
    bool move = false;
    CastController cCastControl;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        cPlayerControl = player.GetComponent<PlayerController>();
        cPlayerStats = player.GetComponent<PlayerStats>();
        cAgent = GetComponent<NavMeshAgent>();
        cTeam = GetComponent<Team>();
        EventManager.StartListening("ENERGY_POINT_CHANGED", OnEnergyPointChanged);
        target = player.transform;
        SetBaseStats(damagePerAttack, attackCooldown, slowTime, slowPower, stunTime);
    }

    public void SetBaseStats(float damage, float attackCooldown, float slowTime, float slowPower, float stunTime)
    {
        this.attackCooldown = attackCooldown;
        attackTimer = new Cooldown(attackCooldown);
        damageTimer = new Cooldown(damagePeriod, false);
        maxAttacksInAttackTime = (int) Mathf.Floor(attackTime / damagePeriod);
        damagePerPeriod = Mathf.Round(damage / maxAttacksInAttackTime);

        this.slowPower = slowPower;
        this.slowTime = slowTime;
        this.stunTime = stunTime;
    }

    void Update()
    {
        if (attemptToGetTargetTimer.Check())
            if (!AttemptToGetTarget())
            {
                target = player.transform;
                damageTimer.SetActive(false);
                SetBeamActive(false);
            }
        FollowTarget();
        AttackControl();
    }

    private void AttackControl()
    {
        if (!attack)
            return;
        if (target == player.transform)
            return;

        if (attackTimer.Check())
        {
            attacksLeft = maxAttacksInAttackTime;
            SetBeamActive(true);
            damageTimer.SetActive(true);
            damageTimer.SetTime(0);
        }
        if (attacksLeft > 0)
        {
            AlignBeamToTarget(target);
            var newDir = Vector3.RotateTowards(transform.forward, target.position - transform.position, 1 * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(newDir);
            if (damageTimer.Check())
            {
                target.GetComponent<Health>().GetDamage(damagePerPeriod, transform, false, DamageType.Familiar, DamageElement.Familiar);
                var targetModifiers = target.GetComponent<CharModifiers>();
                if (slowTime > 0 && targetModifiers != null)
                    targetModifiers.SetValue("familiar", ModifierType.Speed, slowPower, slowTime);
   
                attacksLeft -= 1;
                if(attacksLeft == 0)
                {
                    if (stunTime > 0 && targetModifiers != null)
                        StunTarget(targetModifiers);
                    damageTimer.SetActive(false);
                    SetBeamActive(false);
                }
            }
        }
    }

    private void StunTarget(CharModifiers targetModifiers)
    {
        targetModifiers.SetValue("familiar", ModifierType.DisableAttack, 1, stunTime);
        targetModifiers.SetValue("familiar", ModifierType.DisableCast, 1, stunTime);
        targetModifiers.SetValue("familiar", ModifierType.DisableMovement, 1, stunTime);
    }

    private void SetBeamActive(bool active)
    {
        beamMain.gameObject.SetActive(active);
        beamStart.gameObject.SetActive(active);
        beamEnd.gameObject.SetActive(active);
        beamMain.GetComponent<LineRenderer>().startWidth = 0;
        beamMain.GetComponent<LineRenderer>().endWidth = 0;
    }

    bool AttemptToGetTarget()
    {
        target = cTeam.GetClosestTeamMember(cTeam.GetOppositeTeam(), transform.position, 15);
        if (target != null)
            return true;
        else
            return false;
    }

    private void AlignBeamToTarget(Transform target)
    {
        beamEnd.transform.position = (target.position + Vector3.up) - beamMain.transform.parent.forward * 0.2f;
        beamMain.transform.parent.LookAt(target.position + Vector3.up);
        var distToPlayer = Vector3.Distance(beamMain.transform.parent.position + Vector3.up, target.position + Vector3.up);
        var lineRenderer = beamMain.GetComponent<LineRenderer>();
        lineRenderer.SetPosition(1, new Vector3(0, 0, distToPlayer));
        if (lineRenderer.startWidth < 0.1f)
        {
            lineRenderer.startWidth += Time.deltaTime / 3;
            lineRenderer.endWidth = Mathf.Max(0, lineRenderer.startWidth - 0.03f);
        }
    }

    public void SetForm(int level)
    {
        for(var i = 0; i < forms.Length; i++)
        {
            if (i == level)
                forms[i].SetActive(true);
            else
                forms[i].SetActive(false);
            var formAnimator = forms[level].GetComponent<Animator>();
            if(formAnimator)
            {
                GetComponent<Animator>().runtimeAnimatorController = formAnimator.runtimeAnimatorController;
                GetComponent<Animator>().avatar = formAnimator.avatar;
            }
        }
    }

    void FollowTarget()
    {
        var distanceToTarget = Vector3.Distance(transform.position, target.position);
        
        if (distanceToTarget > maxDistance)
            move = true;
        if (move)
        {
            MovementController.MoveToV3(cAgent, target.position, 0);
            cAgent.speed =  Utility.ChangeValueWithSpeed(cAgent.speed, maxSpeed, acceleration * Time.deltaTime);
        }
            
        if (distanceToTarget < minDistance)
        {
            move = false;
            cAgent.speed =  Utility.ChangeValueWithSpeed(cAgent.speed, 0, breakingSpeed * Time.deltaTime);
        }
    }

    public Transform GetTarget()
    {
        return player.transform;
    }

    void OnEnergyPointChanged()
    {
        var points = EventManager.GetFloat("ENERGY_POINTS");
        var increased = EventManager.GetBool("INCREASED");
        for (var i=0; i < 4; i++)
        {
            if (i <= points)
                energyFeedbackParticles[i].gameObject.SetActive(true);
            else
                energyFeedbackParticles[i].gameObject.SetActive(false);
        }
        if (points == 3)
            energyFeedbackParticles[4].gameObject.SetActive(true); 
        else
            energyFeedbackParticles[4].gameObject.SetActive(false);
        if (increased)
            energyChangeParticle.GetComponent<ParticleSystem>().Play(true);
    }

    public void EnergyEvent(float oldEnergy, float newEnergy)
    {
        var oldEnergyParticles = Mathf.Floor(oldEnergy / 25) - 1;
        var energyParticles = Mathf.Floor(newEnergy / 25) - 1;
        for (var i=0; i < 4; i++)
        {
            if (i <= energyParticles)
                energyFeedbackParticles[i].gameObject.SetActive(true);
            else
                energyFeedbackParticles[i].gameObject.SetActive(false);
        }
        if (energyParticles == 3)
            energyFeedbackParticles[4].gameObject.SetActive(true); 
        else
            energyFeedbackParticles[4].gameObject.SetActive(false);
        if (oldEnergyParticles < energyParticles)
            energyChangeParticle.GetComponent<ParticleSystem>().Play(true);
    }
}
