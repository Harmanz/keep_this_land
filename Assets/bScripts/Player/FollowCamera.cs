﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    PlayerController player;
    Vector3 cameraPos;

    void Start() 
    {
        player= GameObject.FindObjectOfType<PlayerController>();
        cameraPos = transform.position;
        if (!player)
            this.enabled = false;
    }
    void Update()
    {   
        var speed = 40f;
        var min_speed = 0f;
        var max_speed = 1f;
        var posToFollow = player.transform.position;
        cameraPos.x = Utility.ChangeValueWithElasticSpeed(cameraPos.x, posToFollow.x, speed, min_speed, max_speed);
        cameraPos.y = Utility.ChangeValueWithElasticSpeed(cameraPos.y, posToFollow.y, speed, min_speed, max_speed);
        cameraPos.z = Utility.ChangeValueWithElasticSpeed(cameraPos.z, posToFollow.z, speed, min_speed, max_speed);
        transform.position = cameraPos;
    }
}
