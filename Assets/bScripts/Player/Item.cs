﻿using UnityEngine;
using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Items", menuName = "Items/Make new item", order = 2)]
[System.Serializable]
public class Item : ScriptableObject 
{
    [SerializeField] public Sprite iconSprite;
    public int maxLevel = 1;
    [SerializeField] public ItemBonus bonusType;
    [ConditionalField("bonusType", false, ItemBonus.Attack)] public ItemAttackBonus attackBonus;
    [ConditionalField("bonusType", false, ItemBonus.Defense)] public ItemDefenseBonus defenseBonus;
    [ConditionalField("bonusType", false, ItemBonus.Shield)] public ItemShieldBonus shieldBonus;
    [ConditionalField("bonusType", false, ItemBonus.Resources)] public ItemResourcesBonus resourcesBonus;
    [ConditionalField("bonusType", false, ItemBonus.CastParams)] public ItemCastParamBonus castParamsBonus;
    [ConditionalField("bonusType", true, ItemBonus.EventCast)] public ItemBonusValueType bonusValueType;
    [HeaderAttribute ("Bonus/Cast chance")]
    public float startValue;
    public float valuePerLevel;
    [ConditionalField("bonusType", false, ItemBonus.EventCast)] public ItemEventCast eventCastParameters;
    public Item[] attachedItems;

    public KeyValuePair<string, float> GetStatForLevel(int level)
    {
        var key = "";
        if (bonusType == ItemBonus.Attack)
            key = attackBonus.ToString();
        if (bonusType == ItemBonus.Defense)
            key = defenseBonus.ToString();
        if (bonusType == ItemBonus.Shield)
            key = shieldBonus.ToString();
        if (bonusType == ItemBonus.Resources)
            key = resourcesBonus.ToString();
        if (bonusType == ItemBonus.CastParams)
            key = castParamsBonus.ToString();

        return new KeyValuePair<string, float>(key + bonusValueType.ToString(), GetValueForLevel(level));
    }

    public float GetValueForLevel(int level)
    {
        return startValue + Mathf.Max(0, level - 1) * valuePerLevel;
    }
}
[System.Serializable]
public class ItemEventCast
{
    public ItemCastEvent castEvent;
    public float cooldown;
    public Cast cast;
    public bool castWithAttachments = false;
}
public enum ItemBonus { Attack, Defense, Shield, Resources, CastParams, EventCast }
public enum ItemBonusValueType {Add, Mult}
public enum ItemAttackBonus { Damage, DamageToHealthy, DamageToWounded, CritChance, CritDamage, FamiliarDamage }
public enum ItemDefenseBonus { HealthMax, AttackVampirism, DamageIncome, ZeroDamageChance, PlayerSpeed }
public enum ItemShieldBonus { ShieldMax, ShieldChargeDelay, ShieldChargeSpeed}
public enum ItemResourcesBonus { EnergyIncomeAttack, EnergyIncomeDefense, GoldIncome, XPIncome }
public enum ItemCastParamBonus { CastSpeed, CastPower, ProjectileSpeed, EnergySkillsPower, FireDamage, FrostDamage, AirDamage, EarthDamage, ArcaneDamage}
public enum ItemCastEvent { EnemyHit, PlayerHit, ShieldDestroyed, Kill, Crit, Death}




