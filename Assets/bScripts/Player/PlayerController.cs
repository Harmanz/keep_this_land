﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;

public enum ControlType
{
    KeyboardAndMouse,Joystick
}


public class PlayerController : MonoBehaviour
{
    [SerializeField] Camera followCamera = null;
    [SerializeField] SkillBinding[] skills = null;
    GameObject moveDirHelper;
    KeyCode usedCastKey;
    ControlBindings skillControls;
    float RightStickXAxis = 0;
    float RightStickYAxis = 0;
    Vector3 vectorToLookAt;
    Vector3 vectorToMove;
    Vector3 animVelocity = new Vector3(0,0,0);
    NavMeshAgent cAgent;
    CastController cCastControl;
    Ray rayToKeyboardAndMouse;
    MovementController cMovementControl = null;
    CharModifiers cModifiers = null;
    Animator cAnimator = null;
    PlayerStats cStats = null;
    FamiliarController cFamiliar = null;
    void Start()
    {
        //Time.timeScale = 0.5f;
        moveDirHelper = new GameObject("MoveDirHelper");
        cAgent = transform.GetComponent<NavMeshAgent>();
        cCastControl = GetComponent<CastController>();
        cModifiers = GetComponent<CharModifiers>();
        cAnimator = GetComponent<Animator>();
        cMovementControl = GetComponent<MovementController>();
        skillControls = (ControlBindings) Resources.Load("Controls");
        skillControls.SetCodes();
        cStats = GetComponent<PlayerStats>();
        cFamiliar = GameObject.FindGameObjectWithTag("Familiar").GetComponent<FamiliarController>();
        //Application.targetFrameRate = 60; // MY FPS LOCKER
    }

// Update is called once per frame

    private void Update()
    {
        //float fps = 1.0f / Time.deltaTime;
        //Debug.Log(fps);
        //DetectPressedKeyOrButton();
        //vectorToLookAt = GetSightDirectionVector(ControlType.Joystick);
        //vectorToMove = GetVectorToMove(ControlType.Joystick);

        vectorToLookAt = GetSightDirectionVector(ControlType.KeyboardAndMouse);
        vectorToMove = GetVectorToMove(ControlType.KeyboardAndMouse);
        SkillControl();
        transform.LookAt(vectorToLookAt);
        moveDirHelper.transform.position = transform.position;
        moveDirHelper.transform.LookAt(vectorToMove);

        UpdateAnimatorAndSpeed();

    }

    public Vector3 GetDestinationPoint()
    {
        return vectorToMove;
    }

    public void DetectPressedKeyOrButton()
    {
        foreach(KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                Debug.Log("KeyCode down: " + kcode);
        }
    }

    private void SkillControl()
    {
        foreach (SkillBinding skill in skills)
        {
            KeyCode keyDown = skillControls.BindingDown(skill.binding);
            
            if (keyDown != 0 && skill.cast)
            {
                var castCost = skill.cast.GetEnergyCost();
                if (castCost  == 0 && !cCastControl.CastIsCharging() && cCastControl.CastStart(skill.cast))
                    usedCastKey = keyDown; 
                if  (castCost > 0 && castCost <= cStats.EnergyGet())
                    if (cFamiliar.GetComponent<CastController>().CastStart(skill.cast))
                        cStats.EnergyTake(castCost);
            }
        }
        if (cCastControl.CastIsCharging() && Input.GetKeyUp(usedCastKey))
            cCastControl.CastStopCharging();
    }
    

    private Vector3 GetVectorToMove(ControlType sightDirType)
    {
        float shiftSize = 2f;
        float xShift = 0;
        float zShift = 0;

        if (sightDirType == ControlType.KeyboardAndMouse)
        {
            if (Input.GetKey(KeyCode.W))
                zShift += shiftSize;
            if (Input.GetKey(KeyCode.A))
                xShift -= shiftSize;
            if (Input.GetKey(KeyCode.S))
                zShift -= shiftSize;
            if (Input.GetKey(KeyCode.D))
                xShift += shiftSize;
        }
        else
        {
            xShift = Input.GetAxis("Horizontal") * shiftSize;
            zShift = Input.GetAxis("Vertical") * shiftSize;
        }

        Vector3 pointToMove = new Vector3(transform.position.x + xShift, transform.position.y, transform.position.z + zShift);

        if (xShift != 0 || zShift != 0)       
            AttemptToMove(pointToMove, 0);
        else
            cAgent.isStopped = true;

        return(pointToMove);
    }

  void CheckDisables()
    {
        if (cModifiers.IsModifierActive(ModifierType.DisableMovement))
            cMovementControl.CancelMovement();

        if (cModifiers.IsModifierActive(ModifierType.DisableAnim))
            cAnimator.enabled = false;
        else
            cAnimator.enabled = true;
    }

    void AttemptToMove(Vector3 target_pos, float distanceToKeep)
    {
        if (cModifiers.IsModifierActive(ModifierType.DisableMovement))
            cMovementControl.CancelMovement();
        else
            cMovementControl.MoveToV3(target_pos, distanceToKeep);
    }

    private Vector3 GetSightDirectionVector (ControlType sightDirType)
    {
        Vector3 vectorToLookAt = new Vector3();
        if (sightDirType == ControlType.KeyboardAndMouse)
        {        
            rayToKeyboardAndMouse = followCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            var mask = LayerMask.GetMask("Terrain");
            Physics.Raycast(rayToKeyboardAndMouse, out raycastHit, 100f, mask);
            RightStickXAxis =  Utility.ChangeValueWithElasticSpeed(RightStickXAxis, raycastHit.point.x, 3, 0, 0.5f);
            RightStickYAxis =  Utility.ChangeValueWithElasticSpeed(RightStickYAxis, raycastHit.point.z, 3, 0, 0.5f);
            vectorToLookAt = new Vector3(RightStickXAxis, transform.position.y, RightStickYAxis);
        }
        else
        {
            var xAxis = Utility.CutIfNotInRangeAbs(Input.GetAxis("RightStickHorizontal"), 0, 0.2f, 1, 1);
            var yAxis = Utility.CutIfNotInRangeAbs(Input.GetAxis("RightStickVertical"), 0, 0.2f, 1, 1);
            Debug.Log(xAxis.ToString()+"____"+yAxis.ToString());
            float xShift = Utility.RoundToDecimals(xAxis);
            float zShift = Utility.RoundToDecimals(yAxis);
            float alpha = Mathf.Atan(zShift/xShift);
            xShift = Mathf.Cos(alpha)*Mathf.Sign(xShift);
            zShift = Mathf.Sin(alpha)*Mathf.Sign(xShift);
             Debug.Log(xShift.ToString()+"__..__"+zShift.ToString());
            if(xAxis==0)
                zShift=zShift*-1;
            Debug.Log(xShift.ToString()+"__..__"+zShift.ToString());
            RightStickXAxis =  Utility.ChangeValueWithElasticSpeed(RightStickXAxis, xShift, 2, 0, 0.1f);
            RightStickYAxis =  Utility.ChangeValueWithElasticSpeed(RightStickYAxis, zShift, 2, 0, 0.1f);
            vectorToLookAt = new Vector3(transform.position.x + RightStickXAxis, transform.position.y, transform.position.z + RightStickYAxis);
        }

            return vectorToLookAt;
    }


    private void UpdateAnimatorAndSpeed()
    {

        Vector3 velocity = cAgent.velocity;
        Vector3 localVelocity = transform.InverseTransformDirection(velocity);
        
        animVelocity.x = Utility.ChangeValueWithSpeed(animVelocity.x, localVelocity.x,0.1f);
        animVelocity.z = Utility.ChangeValueWithSpeed(animVelocity.z, localVelocity.z,0.1f);

        cAnimator.SetFloat("forwardSpeed", animVelocity.z);
        cAnimator.SetFloat("strafeSpeed", animVelocity.x);

        float angle_difference = Utility.AngleDifferenceAbs(transform.rotation.eulerAngles.y,moveDirHelper.transform.rotation.eulerAngles.y);
        cAnimator.SetFloat("lookMoveAngle", angle_difference);
        //CalculateMovementSpeed(angle_difference);

    }

    private void CalculateMovementSpeed(float angle_difference)
    {
        float normal_speed = 5f;
        float backward_speed = 4f;
        float diff = normal_speed - backward_speed;
        float coeff = 1f - (angle_difference / 180f);
        float final_speed = backward_speed + diff * coeff;
        cAgent.speed = final_speed;
    }

    public SkillBinding[] GetSkills()
    {
        return skills;
    }
    public void SetCastSpellForLevel(Cast cast, int level)
    {
        foreach(var skill in skills)
            if(skill.cast == cast)
            {
                if (level < 1)
                    skill.cast = null;
                else
                {
                    skill.cast = Instantiate(cast);
                    skill.cast.SetLevel(level);
                }
            }
    }
    public void SetCastSpellForLevel(Cast cast, int level, float chargeTime, float spellLifeTimeModifier, float cooldownTime)
    {
        foreach(var skill in skills)
            if(skill.cast == cast)
            {
                if (level < 1)
                    skill.cast = null;
                else
                {
                    skill.cast = Instantiate(cast);
                    skill.cast.SetLevel(level);
                    skill.cast.SetCastBasicBalues(chargeTime, spellLifeTimeModifier, cooldownTime);
                }
            }
    }

    private void OnDrawGizmos() 
    {
        /*Gizmos.color = Color.red;
        Gizmos.DrawSphere(vectorToLookAt, 0.2f);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.5f);
    */
    }

    // ANIMATION EVENTS
    void FootR(){}
    void FootL(){}
}
