﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TigerForge;
using Leguar.TotalJSON;
using System;
using System.Linq;

public class PlayerStats : MonoBehaviour
{
    // Resources
    [SerializeField] float ressurectPoints = 0;
    [SerializeField] float energy = 0;
    [SerializeField] float shield = 0;
    
    float shieldMax = 25;
    float shieldChargeDelay = 3;
    float shieldChargeSpeed = 15;
    float shieldTimeToRecharge = 0;
    float energyIncomeAttack = 0.1f;
    float energyIncomeDefense = 0.1f;
    FamiliarController cFamiliar;
    MovementController cMovement;
    FilesHandler FH;
    public Dictionary<string, float> stats = new Dictionary<string, float>();
    Dictionary<Item, int> items = new Dictionary<Item, int>();
    Dictionary<Item, float> eventCastCooldowns = new Dictionary<Item, float>();
    void Start()
    {
        FH = GameObject.FindGameObjectWithTag("FilesHandler").GetComponent<FilesHandler>();
        cFamiliar = GameObject.FindGameObjectWithTag("Familiar").GetComponent<FamiliarController>();
        cMovement = GetComponent<MovementController>();
        shield = shieldMax;
        StatsCreateDictionary();
        StatsLoadBase();
        StatsCalculate();
        
        var playerController = GetComponent<PlayerController>();
        var playerSkills = playerController.GetSkills();
        foreach (var skill in playerSkills)
        {
            if (skill.binding == eBinding.SkillDamage || skill.binding == eBinding.SkillTest)
                continue;
            var skillName = skill.cast.name.Remove(0,2);
            var skillMainLevel = FH.files[FileType.Progression].GetFloat("Skill_" + skillName + "Main_Level","Skills");
            if (FH.files[FileType.Progression].PathExists("Skill_" + skillName + "Add_Level", "Skills"))
            {
                var skillAddLevel = FH.files[FileType.Progression].GetFloat("Skill_" + skillName + "Add_Level","Skills");
                var castCooldown = FH.files[FileType.Config].GetFloat("Skill_" + skillName + "Add_CastCooldown_" + skillAddLevel.ToString(),"Skills");
                var chargeTime = FH.files[FileType.Config].GetFloat("Skill_" + skillName + "Add_CastTime_" + skillAddLevel.ToString(),"Skills");
                var spellLifeTimeModifier = FH.files[FileType.Config].GetFloat("Skill_" + skillName + "Add_SpellLifetime_" + skillAddLevel.ToString(),"Skills");
                playerController.SetCastSpellForLevel(skill.cast, (int)skillMainLevel, chargeTime, spellLifeTimeModifier, castCooldown);
            }
            else
                playerController.SetCastSpellForLevel(skill.cast, (int)skillMainLevel);
        }
        EventManager.EmitEvent("PLAYER_SKILLS_CHANGED");

        /*var tries = 100000;
        var length = 70;
        var startChance = 0.0f;
        var chanceStep = (1.0f / length);
        var success = 0;
        var chance = startChance;
        var s = tries / length;
        Debug.Log(chanceStep);
        while (tries > 0)
        {
            tries -= 1;
            Debug.Log(chance);
            if (Utility.Chance(chance))
            {
                chance -= 1;
                success += 1;
                
            }
            else
                chance += chanceStep;
        }
        
        Debug.Log(s.ToString());
        Debug.Log(success.ToString());*/
    }
    
    void Update()
    {
        ShieldRecharge();
        EnergyRefillByDamage(100, eTeam.EnemyGeneric);
        EventCastCooldownsProcess();
    }

    private void EventCastCooldownsProcess()
    {
        foreach(Item key in eventCastCooldowns.Keys.ToList())
        {
            if(eventCastCooldowns[key] > 0)
                eventCastCooldowns[key] -= Time.deltaTime;
            else
                eventCastCooldowns.Remove(key);
        }
    }

    public void EventCastCall(ItemCastEvent castEvent, Transform target)
    {
        foreach(var itemAndLevel in items)
        {
            var item = itemAndLevel.Key;

            if (item.bonusType != ItemBonus.EventCast || item.eventCastParameters.castEvent != castEvent)
                continue;
            if (!Utility.Chance(item.GetStatForLevel(itemAndLevel.Value).Value))
                continue;
            if (!EventCastCallCheckCooldown(item))
                continue; 

            item.eventCastParameters.cast.CastSpell(eTeam.Player, Mathf.Infinity, transform, target, 1);
            if (item.eventCastParameters.castWithAttachments)
                foreach (var attachment in item.attachedItems)
                    attachment.eventCastParameters.cast.CastSpell(eTeam.Player, Mathf.Infinity, transform, target, 1);
        }
    }

    public bool EventCastCallCheckCooldown(Item item)
    {
        if (eventCastCooldowns.Keys.Contains(item))
            return false;
        else
        {
            eventCastCooldowns.Add(item, item.eventCastParameters.cooldown);
            if (item.eventCastParameters.castWithAttachments)
                foreach (var attachment in item.attachedItems)
                    eventCastCooldowns.Add(attachment, item.eventCastParameters.cooldown);
            return true;
        }
    }
    private void ShieldRecharge()
    {
        if (shieldTimeToRecharge > 0)
            shieldTimeToRecharge -= Time.deltaTime;
        else
            shield = Mathf.Min(shield + shieldChargeSpeed * Time.deltaTime, shieldMax);
    }

    public float ShieldDamage(float amount)
    {
        shieldTimeToRecharge = shieldChargeDelay;
        var shieldDamage = Mathf.Min(shield, amount);
        if (shield == shieldDamage)
            EventCastCall(ItemCastEvent.ShieldDestroyed, transform);
        shield -= shieldDamage;
        return amount - shieldDamage;
    }

    public float ShieldGetPercent()
    {
        return shield/shieldMax;
    }

    public void EnergyRefillByDamage(float damage, eTeam team)
    {
        var newEnergy = 0f;
        if(team == eTeam.EnemyGeneric)
            newEnergy = Mathf.Min(damage * energyIncomeAttack, 100);
        else
            newEnergy = Mathf.Min(damage * energyIncomeDefense, 100);
        
        EnergySendEvent(energy, newEnergy);
        energy = newEnergy;
    }

    public float EnergyGet()
    {
        return energy;
    }

    public void EnergyTake(float amount)
    {
        var newEnergy = Mathf.Max(energy - amount, 0);
        EnergySendEvent(energy, newEnergy);
            energy = newEnergy;
    }

    private void EnergySendEvent(float energy, float newEnergy)
    {
        var energyPoints = Mathf.Floor(energy / 25) - 1;
        var newEnergyPoints = Mathf.Floor(newEnergy / 25) - 1;
        if (energyPoints != newEnergyPoints)
        {
            var increased =  energyPoints > newEnergyPoints ? false : true;
            EventManager.EmitEventData("ENERGY_POINTS", newEnergyPoints);
            EventManager.EmitEventData("INCREASED", increased);
            EventManager.EmitEvent("ENERGY_POINT_CHANGED");
        }
    }

    private void StatsCreateDictionary()
    {
        foreach (var bonusType in Enum.GetValues(typeof(ItemBonusValueType)))
        {
            foreach (var enum_val in Enum.GetValues(typeof(ItemAttackBonus)))
                stats.Add(enum_val.ToString() + bonusType.ToString(), 0f);
            foreach (var enum_val in Enum.GetValues(typeof(ItemDefenseBonus)))
                stats.Add(enum_val.ToString() + bonusType.ToString(), 0f);
            foreach (var enum_val in Enum.GetValues(typeof(ItemShieldBonus)))
                stats.Add(enum_val.ToString() + bonusType.ToString(), 0f);
            foreach (var enum_val in Enum.GetValues(typeof(ItemResourcesBonus)))
                stats.Add(enum_val.ToString() + bonusType.ToString(), 0f);
            foreach (var enum_val in Enum.GetValues(typeof(ItemCastParamBonus)))
                stats.Add(enum_val.ToString() + bonusType.ToString(), 0f);
        }

        stats.Add("ShieldMaxBase", shieldMax);
        stats.Add("ShieldChargeDelayBase", shieldChargeDelay);
        stats.Add("ShieldChargeSpeedBase", shieldChargeSpeed);
        stats.Add("EnergyIncomeAttackBase", 0.1f);
        stats.Add("EnergyIncomeDefenseBase", 0.05f);
        stats.Add("HealthMaxBase", 100);
        stats.Add("CritChanceBase", 0);
        stats.Add("CritMultBase", 1);
        stats.Add("DamageOutcomeMultBase", 1);
        stats.Add("PlayerSpeedBase", 3.5f);
        stats.Add("CastSpeedBase", 3.5f);
    }

    private void StatsLoadBase()
    {
        stats["ShieldMaxBase"]          = FH.GetFloatFromConfigForSkillLevel("PassiveShield", "ShieldCapacity");
        stats["ShieldChargeDelayBase"]  = FH.GetFloatFromConfigForSkillLevel("PassiveShield", "ChargeDelay");
        stats["ShieldChargeSpeedBase"]  = FH.GetFloatFromConfigForSkillLevel("PassiveShield", "ChargeSpeed");
        stats["HealthMaxBase"]          = FH.GetFloatFromConfigForSkillLevel("MaxHealth", "Amount");
        stats["CritChanceBase"]         = FH.GetFloatFromConfigForSkillLevel("CriticalAttack", "Chance");
        stats["CritMultBase"]           = FH.GetFloatFromConfigForSkillLevel("CriticalAttack", "Mult");
        stats["DamageOutcomeMultBase"]  = FH.GetFloatFromConfigForSkillLevel("DamageIncrease", "Mult");

        StatGetFromConfig("EnergyIncomeAttackBase");
        StatGetFromConfig("EnergyIncomeDefenseBase");
        StatGetFromConfig("PlayerSpeedBase");
        
        StatsLoadFamiliar();
    }

    private void StatsLoadFamiliar()
    {
        var matFamiliarLevel = (int)FH.files[FileType.Progression].GetFloat("Skill_FamiliarMaterial_Level", "Skills");
        cFamiliar.SetForm(matFamiliarLevel);

        var familiarDmgPerAttack    = FH.files[FileType.Config].GetFloat("FamiliarDamagePerAttack", "Stats");
        var familiarAttackCooldown  = FH.GetFloatFromConfigForSkillLevel("FamiliarAttack", "AttackCooldown");
        var familiarSlowPower       = FH.files[FileType.Config].GetFloat("FamiliarSlowPower", "Stats");
        var familiarSlowTime        = FH.GetFloatFromConfigForSkillLevel("FamiliarAttack", "SlowTime");
        var familiarStunTime        = FH.GetFloatFromConfigForSkillLevel("FamiliarAttack", "StunTime");
        cFamiliar.SetBaseStats(familiarDmgPerAttack, familiarAttackCooldown, familiarSlowTime, familiarSlowPower, familiarStunTime);
    }

    private void StatsCalculate()
    {
        var statsCopy = new Dictionary<string, float>(stats);
        foreach (var key in statsCopy.Keys)
            stats[key] = key.EndsWith("Base") ? stats[key] : 0;
        foreach (var itemAndLevel in items)
        {
            if (itemAndLevel.Key.bonusType == ItemBonus.EventCast)
                continue;
            var bonus = itemAndLevel.Key.GetStatForLevel(itemAndLevel.Value);
            stats[bonus.Key] += bonus.Value;
        }    

        shieldMax = StatCombineMultAdd("ShieldMax");
        shieldChargeDelay = StatCombineMultAdd("ShieldChargeDelay");
        shieldChargeSpeed = StatCombineMultAdd("ShieldChargeSpeed");
        energyIncomeAttack = StatCombineMultAdd("EnergyIncomeAttack");
        energyIncomeDefense = StatCombineMultAdd("EnergyIncomeDefense");
        GetComponent<Health>().SetMaxHealth(StatCombineMultAdd("HealthMax"));
        cMovement.SetSpeed(StatCombineMultAdd("PlayerSpeed"));
        cFamiliar.GetComponent<CastController>().SetPower(1 + stats["EnergySkillsPowerMult"]);
        GetComponent<CastController>().SetPower(1 + stats["CastPowerMult"]);
        GetComponent<CastController>().SetSpeedModifier(1 + stats["ProjectileSpeedMult"]);
        GetComponentInChildren<ChargeMarker>().SetCastSpeed(1 + stats["CastSpeedMult"]);
    }

    private void StatGetFromConfig(string key)
    {
        stats[key] = FH.files[FileType.Config].GetFloat(key, "Stats");
    }
    
    private float StatCombineMultAdd(string key)
    {
        return stats[key+"Base"] + (stats[key+"Base"] * stats[key+"Mult"]) + stats[key+"Add"];
    }

    public void ItemAddWithAttachments(Item item, int level)
    {
        ItemAdd(item, level);
        foreach (var attachedItem in item.attachedItems)
            ItemAdd(attachedItem, level);
        StatsCalculate();
        EventManager.EmitEvent("ITEMS_CHANGED");
    }
    private void ItemAdd(Item item, int level)
    {
        if (!items.ContainsKey(item))
            items.Add(item, Mathf.Min(level, item.maxLevel));
        else
            items[item] = Mathf.Min(items[item] + level, item.maxLevel);
    }

    public int ItemGetLevel(Item item)
    {
        if (!items.ContainsKey(item))
            return 0;
        else
            return items[item];
    }

    public Dictionary<Item, int> ItemsGetAll()
    {
        return items;
    }



    

}
