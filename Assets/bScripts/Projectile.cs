﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float spread = 1f;
    Transform owner;
    Cooldown lifeTime = new Cooldown(10f);
    float damage = 10f;
    eTeam enemyTeam = eTeam.EnemyGeneric;
    [SerializeField]  Cooldown destroyTimer = new Cooldown(1.5f, false);

    public void Initialize(Transform owner, Transform target, float damage, float predictChance, eTeam enemyTeam)
    {
        this.owner = owner;
        if (target != null)
        {
            var player = target.GetComponent<PlayerController>();
            var spreadVector = new Vector3(Random.Range(-spread, spread), 0, Random.Range(-spread, spread));
            if (player && Utility.Chance(predictChance))
            {
                transform.LookAt(player.GetDestinationPoint() + new Vector3(0, 1, 0) + spreadVector, new Vector3(0, 0, 0));
            }
            else
                transform.LookAt(target.position + new Vector3(0, 1, 0) + spreadVector, new Vector3(0, 0, 0));
        }
        this.damage = damage;
        SetEnemyTeam(enemyTeam);
    }

    public void SetEnemyTeam(eTeam enemyTeam)
    {
        this.enemyTeam = enemyTeam;
    }
    public eTeam GetEnemyTeam()
    {
        return(enemyTeam);
    }
    // Update is called once per frame
    void Update()
    {
        if(lifeTime.Check())
            Destroy(this.gameObject);
        if(destroyTimer.Check())
            Destroy(this.gameObject);
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other) 
    {
        
        Team otherTeam = other.GetComponent<Team>();
        if (otherTeam != null && otherTeam.GetTeam() == enemyTeam)
        {
            var enemy_catcher = other.GetComponent<ProjectileCatcher>();
            if(enemy_catcher)
                return;
            var collider = GetComponent<Collider>();
            collider.enabled = false;
            var enemy_health = other.GetComponent<Health>();
            if (enemy_health)
                enemy_health.GetDamage(damage, owner, true, DamageType.Projectile, DamageElement.Physics);
            speed = 0;
            transform.parent = Utility.FindCharBodyPart(other.transform, "Root/Hips/Spine_01");
            if (destroyTimer.cd > 0)
                destroyTimer.SetActive(true);
            else
                Destroy(this.gameObject);
        }
    }
    public void Bounce(float rx, float ry, float rz)
    {
        var x_rand = UnityEngine.Random.Range(-rx, rx);
        var y_rand = 180 + UnityEngine.Random.Range(-ry, ry);
        var z_rand = UnityEngine.Random.Range(-rz, rz);
        transform.rotation = Utility.GetAnglesModified(transform.rotation, x_rand, y_rand, z_rand);
    }
}
