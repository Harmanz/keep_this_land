﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCatcher : MonoBehaviour
{
    [SerializeField] GameObject impactParticlePrefab = null;
    [SerializeField] MeshRenderer hitFeedbackMesh = null;
    [SerializeField] bool reflect = false;
    [SerializeField] float preciseReflectChance = 0f;
    float reflectAngle = 30f;
    float preciceReflectAngle = 5f;
    float hitFeedbackAlpha = 1f;
    eTeam myTeam;
    // Start is called before the first frame update
    void Start()
    {
        myTeam = GetComponent<Team>().GetTeam();
    }

    // Update is called once per frame
    void Update()
    {
        if (hitFeedbackMesh)
        {
            hitFeedbackMesh.material.SetColor("_EmissionColor", new Color(hitFeedbackAlpha,hitFeedbackAlpha,hitFeedbackAlpha,hitFeedbackAlpha));
            hitFeedbackAlpha = Utility.ChangeValueWithSpeed(hitFeedbackAlpha,0,Time.deltaTime*1);
        }
    }
    private void OnTriggerEnter(Collider other) 
    {
        var otherProj = other.GetComponent<Projectile>();
        var otherMagic = other.GetComponent<Magic>();
        if((otherProj && otherProj.GetEnemyTeam() == myTeam) || (otherMagic && otherMagic.GetComponent<Team>().GetTeam()!=myTeam && otherMagic.flight.speed > 0))
        {
            hitFeedbackAlpha = 0.2f;
            if(!reflect)
            {
                var particle = Instantiate(impactParticlePrefab, other.transform.position, other.transform.rotation);
                Magic.DestroyParticleAfterPlay(particle);
                if (otherProj)
                    Destroy(other.gameObject);
                else
                    otherMagic.SelfDestroy();
            }
            else
            {
                var particle = Instantiate(impactParticlePrefab, other.transform.position, other.transform.rotation);
                Magic.DestroyParticleAfterPlay(particle);
                if(!Utility.Chance(preciseReflectChance))
                {
                    if (otherProj)
                    {
                        otherProj.Bounce(reflectAngle,reflectAngle,reflectAngle);
                        otherProj.SetEnemyTeam(GetComponent<Team>().GetOppositeTeam());
                    }
                    else
                    {
                        otherMagic.Bounce(reflectAngle,reflectAngle,reflectAngle);
                        otherMagic.GetComponent<Team>().SetTeam(myTeam);
                        otherMagic.GetComponent<MagicCollider>().CollisionRefresh();
                        otherMagic.GetComponent<MagicCollider>().GetTeamFromMagic();
                    }
                }
                else
                {
                    if (otherProj)
                    {
                        otherProj.Bounce(1,preciceReflectAngle,1);
                        otherProj.SetEnemyTeam(GetComponent<Team>().GetOppositeTeam());
                    }
                    else
                    {
                        otherMagic.Bounce(2,preciceReflectAngle,2);
                        otherMagic.GetComponent<Team>().SetTeam(myTeam);
                        otherMagic.GetComponent<MagicCollider>().CollisionRefresh();
                        otherMagic.GetComponent<MagicCollider>().GetTeamFromMagic();
                    }
                }
            }
        }
    }
}
