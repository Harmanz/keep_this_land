﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTween : MonoBehaviour
{
    [SerializeField] Vector3 rotationSpeed = Vector3.zero;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationSpeed * Time.deltaTime, Space.Self);
        //var vectorRot = transform.rotation.eulerAngles;
        //vectorRot.x += rotationSpeed.x * Time.deltaTime;
        //vectorRot.y += rotationSpeed.y * Time.deltaTime;
        //vectorRot.z += rotationSpeed.z * Time.deltaTime;
        //transform.rotation = Quaternion.Euler(vectorRot);

    }
}
