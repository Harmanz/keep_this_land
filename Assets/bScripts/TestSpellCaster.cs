﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpellCaster : MonoBehaviour
{
    [SerializeField] Cast chosenMagic = null;
    [SerializeField] float cooldown = 1f;
    Cooldown spellCooldown = null;
    // Start is called before the first frame update
    void Start()
    {
        spellCooldown = new Cooldown(cooldown);
    }

    // Update is called once per frame
    void Update()
    {
        if (spellCooldown.Check(true))
        {
            chosenMagic.CastSpell(GetComponent<Team>().GetTeam(), chosenMagic.GetChargeTime(), transform, transform, 1);
        }
    }
}
