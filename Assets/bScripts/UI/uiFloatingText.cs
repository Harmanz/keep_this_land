﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiFloatingText : MonoBehaviour
{
    [SerializeField] float lifetime = 2f;
    [SerializeField] Text valueText = null;
    [SerializeField] Color normalDamageColor = Color.yellow;
    [SerializeField] Color critDamageColor = Color.red;
    [SerializeField] Color healColor = Color.green;
    [SerializeField] float horSpeed = 1f;
    [SerializeField] float verSpeed = 1f;
    [SerializeField] int normalFontSize = 25;
    [SerializeField] int critFontSize = 35;
    bool active = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(active)
        {
            transform.eulerAngles = Camera.main.transform.eulerAngles;
            transform.Translate(Vector3.up * verSpeed * Time.deltaTime);
            transform.Translate(Vector3.left * horSpeed * Time.deltaTime);
            horSpeed = Utility.ChangeValueWithSpeed(horSpeed, 0, 1 * Time.deltaTime);
            valueText.color = Utility.ChangeColorAlpha(valueText.color, - Time.deltaTime/lifetime);
            if (valueText.color.a == 0)
                GameObject.Destroy(this.gameObject);
        }
    }

    public void Initialize(float value, bool isCrit)
    {
        active = true;
        valueText.gameObject.SetActive(true);
        valueText.color = (value > 0) ? healColor : (isCrit ? critDamageColor : normalDamageColor);
        valueText.text = Mathf.Abs(value).ToString();
        valueText.fontSize = isCrit ? critFontSize : normalFontSize;
        horSpeed *= Utility.Choose(1, -1);
        transform.eulerAngles = Camera.main.transform.eulerAngles;
        transform.Translate(Vector3.forward * -3);
        transform.Translate(Vector3.up);
    }
}
