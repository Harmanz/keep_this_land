﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiHealthbar : MonoBehaviour
{
    Health target = null;
    [SerializeField] bool playerHealth = true;
    [SerializeField] Image healthBar = null;
    [SerializeField] Image healthBarDark = null;
    // Start is called before the first frame update
    void Start()
    {
        if (playerHealth)
        {
            var player = GameObject.FindGameObjectWithTag("Player");
            target = player.GetComponent<Health>(); 
        }
    }

    // Update is called once per frame
    void Update()
    {
        ProcessHealthBar();
    }

    void ProcessHealthBar()
    {
        if(target == null)
            return;

        if (target.IsDead() && !playerHealth)
        {
            var mainUI = GameObject.FindGameObjectWithTag("UI");
            mainUI.GetComponent<uiMainScreen>().SwitchBossBar();
        }
        var percent = target.GetHealthPercent();
        healthBar.fillAmount = percent;
        healthBarDark.fillAmount = Utility.ChangeValueWithSpeed(healthBarDark.fillAmount, percent, 0.005f);
    }
    
    public void SetTarget(Health target)
    {
        this.target = target;
    }
}
