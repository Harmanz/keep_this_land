﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class uiMainMenu : MonoBehaviour
{
    [SerializeField] uiSimpleButton playBtn = null;
    [SerializeField] uiSimpleButton settingsBtn = null;
    [SerializeField] uiSimpleButton exitBtn = null;
    [SerializeField] uiSimpleButton saveSettingsBtn = null;
    [SerializeField] uiSimpleButton exitSettingsBtn = null;
    [SerializeField] Slider graphicsSlider = null;
    [SerializeField] GameObject settingsMenu = null;
    [SerializeField] GameObject mainMenu = null;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playBtn.IsClicked())
        {
            SceneManager.LoadScene("test_combat_scene", LoadSceneMode.Single);
        }
        if (settingsBtn.IsClicked())
        {
            mainMenu.SetActive(false);
            settingsMenu.SetActive(true);
        }
        if (saveSettingsBtn.IsClicked())
        {
            QualitySettings.SetQualityLevel((int)graphicsSlider.value, true);
            mainMenu.SetActive(true);
            settingsMenu.SetActive(false);
        }
        if (exitSettingsBtn.IsClicked())
        {
            mainMenu.SetActive(true);
            settingsMenu.SetActive(false);
        }
        if (exitBtn.IsClicked())
        {
            //Application.Quit();
            Debug.Log("NUMBERS"+Utility.NumbersWithSum(5,10,4));
        }
    }
}
