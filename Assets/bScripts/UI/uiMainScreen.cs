﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TigerForge;

public class uiMainScreen : MonoBehaviour
{
    PlayerController player;
    PlayerStats playerStats;
    [SerializeField] Texture2D cursorImage = null;
    [SerializeField] RectTransform bossBar = null;
    [SerializeField] Image blackScreen = null;
    [SerializeField] Image energyBar = null;
    [SerializeField] Image energyBarHighlight = null;
    [SerializeField] RectTransform itemBar = null;
    [SerializeField] GameObject itemPrefab = null;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerStats = player.GetComponent<PlayerStats>();
        Cursor.SetCursor(cursorImage, new Vector2(0,10), CursorMode.ForceSoftware);
        EventManager.StartListening("ENERGY_POINT_CHANGED", OnEnergyPointChanged);
        EventManager.StartListening("ITEMS_CHANGED", OnItemsChanged);
    }

    // Update is called once per frame
    void Update()
    {
        if (blackScreen.color.a > 0)
            blackScreen.color = Utility.ChangeColorAlpha(blackScreen.color, -0.5f * Time.deltaTime);
        
        if (energyBarHighlight.color.a > 0)
            energyBarHighlight.color = Utility.ChangeColorAlpha(energyBarHighlight.color, -2f * Time.deltaTime);

        energyBar.fillAmount = Utility.ChangeValueWithElasticSpeed(energyBar.fillAmount, playerStats.EnergyGet()/100, 20, 0.005f, 1);
    }

    void OnEnergyPointChanged()
    {
        energyBarHighlight.color = Utility.ChangeColorAlpha(energyBarHighlight.color, 1);
    }

    void OnItemsChanged()
    {
        for (var i = itemBar.childCount - 1; i >= 0; i--)
            GameObject.Destroy(itemBar.GetChild(i).gameObject);
        var items = playerStats.ItemsGetAll();
        var shiftBetweenItems = Mathf.Min(50, 550 / items.Count);
        var itemCount = 1;
        
        foreach (var itemEntry in items)
        {
            if (itemEntry.Key.iconSprite == null)
                continue;
            var uiItem = Instantiate(itemPrefab, itemBar);
            uiItem.GetComponent<RectTransform>().localPosition = new Vector3(shiftBetweenItems * (itemCount - 1), 0, 0);
            uiItem.GetComponent<uiMainScreenItem>().icon.sprite = itemEntry.Key.iconSprite;
            uiItem.GetComponent<uiMainScreenItem>().levelText.text = itemEntry.Value.ToString();
            itemCount++;
        }
    }

    public void SetBlackScreenAlpha(float alpha)
    {
        var col = blackScreen.color;
        col.a = alpha;
        blackScreen.color = col;
    }
    public void SwitchBossBar(Health boss)
    {
        bossBar.gameObject.SetActive(true);
        bossBar.GetComponent<uiHealthbar>().SetTarget(boss);
    }
    public void SwitchBossBar()
    {
        bossBar.gameObject.SetActive(false);
    }
}
