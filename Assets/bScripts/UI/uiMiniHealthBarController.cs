﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class uiMiniHealthBarController : MonoBehaviour
{
    [SerializeField] Image bg = null;
    [SerializeField] Image hp_main = null;
    [SerializeField] Image hp_damaged = null;
    float health = 1f;
    float health_current = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles = Camera.main.transform.eulerAngles;
        health_current = Utility.ChangeValueWithElasticSpeed(health_current, health, 10, 0.01f, 1f);
        hp_main.rectTransform.sizeDelta = new Vector2(health*100, hp_main.rectTransform.sizeDelta.y);
        hp_damaged.rectTransform.sizeDelta = new Vector2(health_current*100, hp_main.rectTransform.sizeDelta.y);
    }
    public void SetHealth(float health)
    {
        this.health = health;
    }
}
