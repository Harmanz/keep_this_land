﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TigerForge;

public class uiProgressionScreen : MonoBehaviour
{
    private const int shiftBetweenDescriptions = 20;
    [SerializeField] GameObject descriptionLayer = null;
    [SerializeField] Image skillIcon = null;
    [SerializeField] Image nextSkillBG = null;
    [SerializeField] Text skillTitle = null;
    [SerializeField] Text skillDescription = null;
    [SerializeField] Text skillLevelNum = null;
    [SerializeField] Text skillPointsCounter = null;
    [SerializeField] GameObject learnSkillBtnObj = null;
    [SerializeField] uiProgressionSkill firstSkill = null;
    uiSimpleButton learnSkillButton;
    FilesHandler FH;
    string skillName;
    GameObject clickedSkill;
    void Start()
    {
        FH = GameObject.FindGameObjectWithTag("FilesHandler").GetComponent<FilesHandler>();
        EventManager.StartListening("PROGRESSION_SKILL_CLICKED", OnProgressionSkillClicked);
        learnSkillButton = learnSkillBtnObj.GetComponent<uiSimpleButton>();
        skillPointsCounter.text = GetSkillPoints().ToString();
        ProcessSkillClick(firstSkill.GetSkillName(), firstSkill.gameObject);
        firstSkill.SetChosen(true);
    }

    void Update()
    {
        ProcessExitToLobby();

        if (learnSkillButton && learnSkillButton.IsClicked())
        {
            var currentLevel = FH.files[FileType.Progression].GetFloat("Skill_" + skillName + "_Level", "Skills");
            var skillPrice = FH.files[FileType.Config].GetFloat("Skill_" + skillName + "_Price_" + (currentLevel + 1).ToString(), "Skills");
            if (skillPrice <= GetSkillPoints())
            {
                FH.files[FileType.Progression].SetValue("SkillPoints", GetSkillPoints() - skillPrice, "Resources");
                FH.files[FileType.Progression].SetValue("Skill_" + skillName + "_Level", currentLevel + 1, "Skills");
                skillPointsCounter.text = GetSkillPoints().ToString();
                LoadSkillInfo(skillName, clickedSkill);
            }
        }
    }

    private void ProcessExitToLobby()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            FH.SaveJSToFile(FileType.Progression);
            EventManager.EmitEventData("SCENE_NAME", "lobby");
            EventManager.EmitEvent("SCENE_LOAD_START");
        }
    }

    void OnProgressionSkillClicked()
    {
        var ev_skill_name = EventManager.GetString("SKILL_NAME");
        var ev_clicked_skill = EventManager.GetGameObject("CLICKED_SKILL");
        ProcessSkillClick(ev_skill_name, ev_clicked_skill);
    }

    private void ProcessSkillClick(string ev_skill_name, GameObject ev_clicked_skill)
    {
        skillName = ev_skill_name;
        clickedSkill = ev_clicked_skill;
        LoadSkillInfo(skillName, clickedSkill);
    }

    void LoadSkillInfo(string skill_name, GameObject skill_obj)
    {
        var skillNameString = "Skill_" + skill_name;

        DestroyDescriptionStrings();

        var icon = skill_obj.GetComponent<uiProgressionSkill>().GetSkillIcon();
        skillIcon.sprite = icon;

        if (FH.files[FileType.Config].PathExists(skillNameString + "_MaxLevel", "Skills"))
        {
            skillTitle.text = FH.GetText(FileType.Localization, skillNameString + "_Title").ToUpper();
            var maxLevel = FH.files[FileType.Config].GetFloat(skillNameString + "_MaxLevel", "Skills");
            var currentLevel = FH.files[FileType.Progression].GetFloat(skillNameString + "_Level", "Skills");

            ProcessLearnButtonAvailability(skill_name, maxLevel, currentLevel);

            var descriptionEntity = skillDescription;
            var levelNumEntity = skillLevelNum;
            nextSkillBG.rectTransform.sizeDelta = new Vector2(0, 0);

            for (var i = 1; i <= maxLevel; i++)
            {
                descriptionEntity.gameObject.name = "level_descr_" + i.ToString();
                descriptionEntity.text = FH.GetText(FileType.Localization, skillNameString + "_" + i.ToString() + "_Descr");
                levelNumEntity.gameObject.name = "level_num_" + i.ToString();
                levelNumEntity.text = i.ToString() + ".";

                ProcessNextSkillBackground(currentLevel, descriptionEntity, i);

                descriptionEntity.color = levelNumEntity.color = GetColorForSkillLevelDescription(currentLevel, i);

                if (i < maxLevel)
                {
                    var shift = new Vector2(0, descriptionEntity.preferredHeight + shiftBetweenDescriptions);
                    descriptionEntity = (Text)DuplicateWithShift(descriptionEntity, - shift);
                    levelNumEntity = (Text)DuplicateWithShift(levelNumEntity, - shift);
                }
            }
        }
        else
        {
            skillTitle.text = skill_name + "_NOT_FOUND";
            learnSkillBtnObj.SetActive(false);
        }
    }

    private void DestroyDescriptionStrings()
    {
        for (var i = 2; i < 10; i++)
        {
            var child = descriptionLayer.transform.Find("skill_description_back/level_descr_" + i.ToString());
            if (child)
                GameObject.Destroy(child.gameObject);
            child = descriptionLayer.transform.Find("skill_description_back/level_num_" + i.ToString());
            if (child)
                GameObject.Destroy(child.gameObject);
        }
    }

    private void ProcessNextSkillBackground(float currentLevel, Text descriptionEntity, int i)
    {
        if (i == (currentLevel + 1))
        {
            nextSkillBG.rectTransform.sizeDelta = new Vector2(1000, descriptionEntity.preferredHeight + shiftBetweenDescriptions);
            nextSkillBG.rectTransform.anchoredPosition = descriptionEntity.rectTransform.anchoredPosition + new Vector2(0,  shiftBetweenDescriptions / 2 - nextSkillBG.rectTransform.rect.height / 2);
        }  
    }

    private void ProcessLearnButtonAvailability(string skill_name, float maxLevel, float currentLevel)
    {
        if (maxLevel != currentLevel)
        {
            learnSkillBtnObj.SetActive(true);
            var skillPrice = FH.files[FileType.Config].GetFloat("Skill_" + skill_name  + "_Price_" + (currentLevel + 1).ToString(), "Skills");
            learnSkillButton.SetClickable(skillPrice <= GetSkillPoints());
        }
        else
            learnSkillBtnObj.SetActive(false);
    }

    private static Graphic DuplicateWithShift(Graphic uiEntity, Vector2 shift)
    {
        uiEntity = Instantiate(uiEntity.gameObject, uiEntity.rectTransform.parent).GetComponent<Text>();
        uiEntity.rectTransform.anchoredPosition = new Vector2(uiEntity.rectTransform.anchoredPosition.x + shift.x, uiEntity.rectTransform.anchoredPosition.y + shift.y);
        return uiEntity;
    }

    private static Color GetColorForSkillLevelDescription(float currentLevel, int i)
    {
        var col = Color.white;
        if (i == (currentLevel + 1))
            col = Color.yellow;
        else if (i > (currentLevel + 1))
            col = Color.gray;
        return col;
    }

    private float GetSkillPoints()
    {
        return FH.files[FileType.Progression].GetFloat("SkillPoints", "Resources");
    }

}
