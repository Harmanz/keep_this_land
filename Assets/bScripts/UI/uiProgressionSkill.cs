﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TigerForge;

public class uiProgressionSkill : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] Image skillIcon = null;
    [SerializeField] Image chosenOutline = null;
    [SerializeField] Image hoverPatch = null;
    [SerializeField] string skill = "skill_name";
    bool hover = false;
    bool chosen = false;

    void Start()
    {
        EventManager.StartListening("PROGRESSION_SKILL_CLICKED", OnProgressionSkillClicked);
    }

    // Update is called once per frame
    void Update()
    {
        ProcessStates();
        ProcessClick();
    }

    private void ProcessClick()
    {
        if (Input.GetMouseButtonUp(0) && hover)
        {
            EventManager.EmitEventData("SKILL_NAME", skill);
            EventManager.EmitEventData("CLICKED_SKILL", gameObject);
            EventManager.EmitEvent("PROGRESSION_SKILL_CLICKED");
        }
    }

    private void ProcessStates()
    {
        hoverPatch.gameObject.SetActive(hover);
        chosenOutline.gameObject.SetActive(chosen);
    }

    void OnProgressionSkillClicked()
    {
        var skillName = EventManager.GetString("SKILL_NAME");
        var clickedSkill = EventManager.GetGameObject("CLICKED_SKILL");
        chosen = gameObject == clickedSkill ? true : false;
    }
    public void SetChosen(bool chosen)
    {
        this.chosen = chosen;
    }
    public string GetSkillName()
    {
        return skill;
    }
    public Sprite GetSkillIcon()
    {
        return skillIcon.sprite;
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        hover = true;
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        hover = false;
    }

    private void OnValidate() 
    {
        gameObject.name = skill;
    }
}
