﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiShieldBar : MonoBehaviour
{
    PlayerStats stats = null;
    [SerializeField] Image bar = null;
    [SerializeField] Image barDark = null;
    // Start is called before the first frame update
    void Start()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        stats = player.GetComponent<PlayerStats>(); 
    }

    // Update is called once per frame
    void Update()
    {
        ProcessHealthBar();
    }

    void ProcessHealthBar()
    {
        if(stats == null)
            return;


        var percent = stats.ShieldGetPercent();
        bar.fillAmount = percent;
        barDark.fillAmount = Utility.ChangeValueWithSpeed(barDark.fillAmount, percent, 0.005f);
    }
    
}
