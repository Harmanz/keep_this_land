﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class uiSimpleButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] string caption = "default";
    [SerializeField] Image normalState = null;
    [SerializeField] Image hoveredState = null;
    [SerializeField] Image pressedState = null;
    bool hover = false;
    bool pressed = false;
    bool clicked = false;
    bool clickable = true;

    // Start is called before the first frame update
    void Start()
    {
        transform.Find("caption").GetComponent<Text>().text = caption;
    }

    // Update is called once per frame
    void Update()
    {   
        if (clickable)
        {
            normalState.color = Utility.SetColorAlpha(normalState.color, 1f);
            hoveredState.gameObject.SetActive(hover);
            pressedState.gameObject.SetActive(pressed);

            if(Input.GetMouseButtonDown(0) && hover)
                pressed = true;
            if(Input.GetMouseButtonUp(0))
            {
                pressed = false;
                clicked = hover;
            }
        }
        else
        {
            hoveredState.gameObject.SetActive(false);
            pressedState.gameObject.SetActive(false);
            normalState.color = Utility.SetColorAlpha(normalState.color, 0.3f);
            clicked = pressed = hover = false;
        }

    }
    public void SetClickable(bool clickable)
    {
        this.clickable = clickable;
    }
    private void OnEnable() 
    {
        hover = false;
        pressed = false;
    }
    public bool IsClicked()
    {
        var isClicked = clicked;
        clicked = false;
        return (isClicked);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        hover = true;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        hover = false;
    }
}
