﻿using System.Collections;
using System.Collections.Generic;
using TigerForge;
using UnityEngine;
//using UnityEngine.UIElements;
using UnityEngine.UI;

public class uiSkillIcon : MonoBehaviour
{
    [SerializeField] eBinding castType = eBinding.None;
    Cast cast;
    [SerializeField] Image icon = null;
    [SerializeField] Image frame = null;
    [SerializeField] Image iconBlack = null;
    [SerializeField] Image frameCharged = null;
    [SerializeField] RectTransform skillButtonTip = null;
    [SerializeField] float requiredEnergy = 0;
    PlayerController player;
    PlayerStats playerStats;
    CastController playerCC;
    float cooldown = 0f;
    int animPhase = -1;
    // Start is called before the first frame update
    void Start()
    {
        GetPlayerAndCast();
        SetSkillButtonTip(castType);
        SwitchChargedFrameAlpha(0f, 1f);
        SetRequiredEnergy();
        EventManager.StartListening("PLAYER_SKILLS_CHANGED", OnPlayerSkillsChanged);
    }

    void OnPlayerSkillsChanged()
    {
        GetPlayerAndCast();
    }

    private void GetPlayerAndCast()
    {
        player = null;
        player = GameObject.FindObjectOfType<PlayerController>();
        if (player)
        {
            playerStats = player.GetComponent<PlayerStats>();
            playerCC = player.GetComponent<CastController>();
            cast = GetCastFromPlayer(player, castType);
            if (cast)
            {
                cooldown = cast.GetCooldown();
                icon.sprite = cast.GetCastIcon();
            }
            else
                gameObject.SetActive(false);
        }
    }

    private void SetRequiredEnergy()
    {
        if (castType == eBinding.ChargedFirst)
            requiredEnergy = 25;
        else if (castType == eBinding.ChargedSecond)
            requiredEnergy = 50;
        else if (castType == eBinding.ChargedThird)
            requiredEnergy = 75;
        else if (castType == eBinding.ChargedFourth)
            requiredEnergy = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (!player || !cast)
            return;

        var timeToCharge = playerCC.GetCastCooldown(cast);
        if (timeToCharge > 0 || (playerStats.EnergyGet() < requiredEnergy && requiredEnergy > 0))
        {
            animPhase = 0;
            SetImageAlpha(icon, 0.5f);
            SetImageAlpha(frame, 1f);
            iconBlack.fillAmount = timeToCharge / cooldown;
        }
        else
        {
            if (animPhase == 0)
            {

                iconBlack.fillAmount = 0f;
                SetImageAlpha(icon, 1f);
                SetImageAlpha(frame, 1f);
                animPhase = 1;
            }
        }
        RechargeAnim();
    }


    private Cast GetCastFromPlayer(PlayerController player, eBinding castBinding)
    {
        foreach (var skill in player.GetSkills())
            if (skill.binding == castBinding)
                return skill.cast;
        return null;
    }

    private void SetSkillButtonTip(eBinding castBinding)
    {
        var skillKey = GetSkillKey(castBinding);
        if(skillKey.Length == 1)
        {
            var text = skillButtonTip.Find("text");
            text.GetComponent<Text>().text = skillKey;
        }
        else
        {
            var icon = skillButtonTip.Find(skillKey);
            icon.gameObject.SetActive(true);
            var text = skillButtonTip.Find("text");
            text.gameObject.SetActive(false);
            var bg = skillButtonTip.Find("bg");
            bg.gameObject.SetActive(false);
        }
    }
    private string GetSkillKey(eBinding castBinding)
    {
        var skillControls = (ControlBindings) Resources.Load("Controls");
        foreach(var binding in skillControls.GetBindings())
            if (binding.bindingName == castBinding)
                return binding.buttons[0].Replace("Alpha","");
        return "";
    }
    bool SwitchChargedFrameAlpha(float value, float speed)
    {
        var col = frameCharged.color;
        col.a = Utility.ChangeValueWithSpeed(col.a, value, speed);
        frameCharged.color = col;
        if (col.a == value)
            return true;
        else
            return false;
    }

    void SetImageAlpha(Image image, float alpha)
    {
        var col = frameCharged.color;
        col.a = alpha;
        image.color = col;
    }
    void RechargeAnim()
    {
        if (requiredEnergy == 0)
        {
            if (animPhase == 0)
            {
                SwitchChargedFrameAlpha(0f, 0.05f);
            }
            if (animPhase == 1)
            {
                if(SwitchChargedFrameAlpha(1f, 0.1f))
                    animPhase = 2;
            }
            if (animPhase == 2)
            {
                if(SwitchChargedFrameAlpha(0f, 0.03f))
                    animPhase = -1;
            }
        }
        else
        {
            if (requiredEnergy <= playerStats.EnergyGet())
                SetImageAlpha(frameCharged, 1f);
            else
                SetImageAlpha(frameCharged, 0f);
        }
    }
}
