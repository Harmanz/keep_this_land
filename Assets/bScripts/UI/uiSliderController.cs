﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiSliderController : MonoBehaviour
{
    [SerializeField] Text caption = null;
    string[] graphicSettingsCaption = new string[7];
    void Start()
    {
        graphicSettingsCaption[0] = "";
        graphicSettingsCaption[1] = "Graphics: very low";
        graphicSettingsCaption[2] = "Graphics: low";
        graphicSettingsCaption[3] = "Graphics: medium";
        graphicSettingsCaption[4] = "Graphics: high";
        graphicSettingsCaption[5] = "Graphics: very high";
        graphicSettingsCaption[6] = "Graphics: ultra";
        GetComponent<Slider>().value = QualitySettings.GetQualityLevel();
    }

    // Update is called once per frame
    void Update()
    {
        var value = (int)GetComponent<Slider>().value;
        caption.text = graphicSettingsCaption[value];
    }
}
