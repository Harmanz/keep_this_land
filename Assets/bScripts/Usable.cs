﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;
using UnityEngine.UI;
using TigerForge;

public class Usable : MonoBehaviour
{

    [SerializeField] string key = "";
    [SerializeField] Image popUpRoot = null;
    [SerializeField] float popUpShowDistance = 1f;
    [SerializeField] Text description = null;
    [SerializeField] UsableType type = UsableType.LoadScene;
    [ConditionalField("type", false, UsableType.LoadScene)] public string sceneName;
    Transform player;
    FilesHandler filesHandler;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        filesHandler = GameObject.FindGameObjectWithTag("FilesHandler").GetComponent<FilesHandler>();
        description.text = key;
    }

    // Update is called once per frame
    void Update()
    {
        popUpRoot.transform.eulerAngles = Camera.main.transform.eulerAngles;
        if (Vector3.Distance(transform.position, player.position) > popUpShowDistance)
            popUpRoot.gameObject.SetActive(false);
        else
        {
            if(key == description.text)
                LoadDataFromItem();
            popUpRoot.gameObject.SetActive(true);
            if (Input.GetKey(KeyCode.F))
                Use();
        }
    }

    private void Use()
    {
        if (type == UsableType.LoadScene)
        {
            EventManager.EmitEventData("SCENE_NAME", sceneName);
            EventManager.EmitEvent("SCENE_LOAD_START");
        }
    }

    private void LoadDataFromItem()
    {
        description.text = filesHandler.GetText(FileType.Localization, "Usable_" + key + "_Description");
        popUpRoot.rectTransform.sizeDelta = new Vector2(Mathf.Max(250, description.preferredWidth), description.preferredHeight + 40);
    }
}

public enum UsableType {LoadScene}